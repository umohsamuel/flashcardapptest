import { EPdfProcessorState } from "@/redux/reducers/pdfProcessor.slice";
import { useAppSelector } from "./redux.hooks";
import { useDispatch } from "react-redux";
import { useClassId } from "./navigation.hooks";
import { reduxActions } from "@/redux/actions";
import { useAppContext } from "@/context/AppContext";
import { useGraphQLClass } from "@/graphql/domain/class.graphql";
import { AxiosResponse } from "axios";
import { useGraphQLDeck } from "@/graphql/domain/deck.graphql";
import { EPdfUploadVerboseOptions } from "@/constants/api.constants";

export function usePdfProcessorState() {
  const { uploadState, lastResponse } = useAppSelector((state) => state.pdfProcessor);
  // Define boolean variables for each potential state for easier consumption
  const isReady = uploadState === EPdfProcessorState.ready;
  const isProcessing = uploadState === EPdfProcessorState.processing;
  const isAborted = uploadState === EPdfProcessorState.aborted;
  const isError = uploadState === EPdfProcessorState.error;
  const isCompleted = uploadState === EPdfProcessorState.completed;

  return {
    isReady,
    isProcessing,
    isAborted,
    isError,
    isCompleted,
    lastResponse,
  };
}

export function useUploadPdf(classIdParam?: string) {
  const dispatch = useDispatch();
  const classId = useClassId() ?? classIdParam;
  const { refetch: refetchClass } = useGraphQLClass(classId);
  const { refetch: refetchDeck } = useGraphQLDeck();
  const { axiosRequestManager } = useAppContext();

  const handleUpload = async (
    file: File,
    params: {
      amount?: number;
      verbose?: EPdfUploadVerboseOptions;
      onError?: (message: string, response?: AxiosResponse<IProcessPdfResponse>) => void;
      onSuccess?: (message: string, response?: AxiosResponse<IProcessPdfResponse>) => void;
    },
  ) => {
    const formData = new FormData();
    formData.append("file", file);
    formData.append("amount", params.amount ? params.amount.toString() : "5");
    formData.append("verbose", params.verbose ?? EPdfUploadVerboseOptions.MEDIUM);
    if (classId) {
      formData.append("classId", classId);
    }

    const setReduxUploadState = (uploadState: EPdfProcessorState) => {
      dispatch(
        reduxActions.pdfProcessor.setUploadState({
          uploadState,
        }),
      );
    };

    setReduxUploadState(EPdfProcessorState.processing);

    try {
      // TODO: use cancel for request aborting
      const { request } = await axiosRequestManager.postUploadPdf(formData);
      const response = await request;
      if (typeof response.data === "string") {
        try {
          response.data = JSON.parse(response.data);
        } catch (e) {
          // toast.error("Cannot process, please retry.");
          params.onError?.("Cannot process, please retry.", response);
          return response;
        }
      }

      if (response.data.error) {
        params.onError?.(response.data.error, response);
        setReduxUploadState(EPdfProcessorState.error);
        return response;
      }

      const { success } = response.data;
      if (success === true) {
        // setReduxUploadState(EPdfProcessorState.completed);
        dispatch(
          reduxActions.pdfProcessor.setUploadState({
            uploadState: EPdfProcessorState.completed,
            lastResponse: response.data,
          }),
        );
        params.onSuccess?.(response.data.statusMessage ?? "Processed successfully.", response);
        refetchClass();
        refetchDeck();
        return response;
      } else if (success === false) {
        params.onError?.(
          `${response.data.statusMessage} please retry.` ??
            "Error with PDF-Processing, please retry.",
          response,
        );
        setReduxUploadState(EPdfProcessorState.error);
      }
    } catch (error) {
      console.error("Error uploading file: ", error);
      params.onError?.(`Error uploading file. ${error}`);
      setReduxUploadState(EPdfProcessorState.error);
    }
  };

  return {
    handleUpload,
  };
}
