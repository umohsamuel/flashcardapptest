import { IRouteParams, ROUTES_CONFIG } from "@/constants/routes.constants";
import { INavigationPropertiesState } from "../redux/reducers/navigation.slice";
import { useAppSelector } from "./redux.hooks";
import { useParams } from "next/navigation";

export function useSpaceId() {
  const params = useParams() as IRouteParams;
  return params.spaceId ?? params.studySpaceId;
}

export function useClassId() {
  const params = useParams() as IRouteParams;
  return params.classId ?? params.studyClassId;
}

export function useDeckId() {
  const params = useParams() as IRouteParams;
  return params.deckId ?? params.studyDeckId;
}

export function useGetDeckRoute() {
  const classId = useClassId();
  const spaceId = useSpaceId();
  return (deckId: string) => ROUTES_CONFIG.getDeckRoute({ spaceId, classId, deckId });
}

export function useNavigationState(): INavigationPropertiesState {
  return useAppSelector((state) => state.navigation.properties);
}

export function useStudyModeNavigationParams() {
  const { studySpaceId, studyClassId, studyDeckId, studyFlashcardId } = useParams();
  return { studySpaceId, studyClassId, studyDeckId, studyFlashcardId };
}
