import { useRouter } from "next/navigation";
import { useClassId, useSpaceId } from "./navigation.hooks";
import { ROUTES_CONFIG } from "@/constants/routes.constants";

interface IUseStudyModeOptions {
  spaceId?: string;
  classId?: string;
  deckId: string;
  flashcardId: string;
}

export function useStudyMode() {
  const router = useRouter();
  const spaceIdConext = useSpaceId();
  const classIdContext = useClassId();

  const startStudying = (options: IUseStudyModeOptions) => {
    const route = ROUTES_CONFIG.getStudyFlashcardRoute({
      spaceId: (options.spaceId ?? spaceIdConext) as string,
      classId: (options.classId ?? classIdContext) as string,
      deckId: options.deckId,
      flashcardId: options.flashcardId,
    });
    router.push(route);
  };

  return { startStudying };
}
