import { configureStore, ThunkAction, Action } from "@reduxjs/toolkit";
import { alertSlice } from "./reducers/alert.slice";
import { navigationSlice } from "./reducers/navigation.slice";
import { pdfProcessorSlice } from "./reducers/pdfProcessor.slice";

export const store = configureStore({
  reducer: {
    alert: alertSlice.reducer,
    navigation: navigationSlice.reducer,
    pdfProcessor: pdfProcessorSlice.reducer,
  },
});

export type AppStore = typeof store;
export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
