"use client"
import { Provider } from "react-redux"
import { store } from "./store"

function OurProvider({ children }: React.PropsWithChildren) {
  return <Provider store={store}>{children}</Provider>;
}

export default OurProvider