import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface IAlertSliceState {
  properties: IAlertPropertiesState;
}

enum EAlertType {
  danger = "danger",
  info = "info",
}

interface IAlertPropertiesState {
  show: boolean;
  type: EAlertType;
  text: string;
}

export const initialState: IAlertSliceState = {
  properties: {
    show: false,
    type: EAlertType.info,
    text: "",
  },
};

export const alertSlice = createSlice({
  name: "alert",
  initialState,
  reducers: {
    setAlertState(state, action: PayloadAction<Partial<IAlertPropertiesState>>) {
      state.properties = {
        ...state.properties,
        ...action.payload,
      };
    },
    resetAlert() {
      return initialState;
    },
  },
});
