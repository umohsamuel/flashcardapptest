import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export enum EPdfProcessorState {
  ready = "ready",
  processing = "processing",
  aborted = "aborted",
  error = "error",
  completed = "completed",
}

export interface IPdfProcessorSliceState {
  uploadState: EPdfProcessorState;
  lastResponse: IProcessPdfResponse | null;
}

export const initialState: IPdfProcessorSliceState = {
  uploadState: EPdfProcessorState.ready,
  lastResponse: null,
};

export const pdfProcessorSlice = createSlice({
  name: "pdfProcessor",
  initialState,
  reducers: {
    setUploadState(
      state,
      action: PayloadAction<{
        uploadState: EPdfProcessorState;
        lastResponse?: IProcessPdfResponse;
      }>,
    ) {
      const { lastResponse, uploadState } = action.payload;
      state.uploadState = action.payload.uploadState;
      if (uploadState === EPdfProcessorState.ready) {
        state.lastResponse = null;
      } else if (lastResponse) {
        state.lastResponse = lastResponse;
      }
    },
    setErrorState(state) {
      state.uploadState = EPdfProcessorState.error;
    },
    resetUploadState() {
      return initialState;
    },
  },
});
