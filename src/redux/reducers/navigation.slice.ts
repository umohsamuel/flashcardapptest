import { createSlice, PayloadAction } from "@reduxjs/toolkit";
// import { HYDRATE } from "next-redux-wrapper";

interface INavigationSliceState {
  properties: INavigationPropertiesState;
}

export interface INavigationPropertiesState {
  isAsideOpened: boolean;
}

export const initialState: INavigationSliceState = {
  properties: {
    isAsideOpened: false,
  },
};

export const navigationSlice = createSlice({
  name: "navigation",
  initialState,
  reducers: {
    setState(state, action: PayloadAction<Partial<INavigationPropertiesState>>) {
      state.properties = {
        ...state.properties,
        ...action.payload,
      };
    },
  },
});
