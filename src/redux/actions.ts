import { alertSlice } from "./reducers/alert.slice";
import { navigationSlice } from "./reducers/navigation.slice";
import { pdfProcessorSlice } from "./reducers/pdfProcessor.slice";

export const reduxActions = {
  alert: alertSlice.actions,
  navigation: navigationSlice.actions,
  pdfProcessor: pdfProcessorSlice.actions,
};
