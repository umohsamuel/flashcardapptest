import { API_ROUTES_CONFIG } from "@/constants/routes.constants";
import axios, { Axios, AxiosRequestConfig, AxiosResponse, CancelTokenSource } from "axios";

const BACKEND_HOST = process.env.NEXT_PUBLIC_BACKEND_HOST || "http://127.0.0.1:8080";

// Define the enum for request types
export enum IRequestType {
  UPLOAD_PDF,
}

export class AxiosRequestManager extends Axios {
  // A map to hold CancelTokenSources for each request type
  private cancelTokenMap: Map<IRequestType, CancelTokenSource> = new Map();

  constructor() {
    super({ baseURL: BACKEND_HOST });
  }

  private getCancelTokenSource(type: IRequestType): CancelTokenSource {
    if (!this.cancelTokenMap.has(type)) {
      const source = axios.CancelToken.source();
      this.cancelTokenMap.set(type, source);
    }
    return this.cancelTokenMap.get(type) as CancelTokenSource;
  }

  public post<T = any, R = AxiosResponse<T>, D = any>(
    url: string,
    data?: D,
    config?: AxiosRequestConfig<D>,
    requestType?: IRequestType,
  ): Promise<R> {
    if (requestType && config) {
      const source = this.getCancelTokenSource(requestType);
      config.cancelToken = source.token;
    }
    return super.post(url, data, config);
  }

  // Cancels the specific type of request
  public cancel(requestType: IRequestType, message: string = "Request canceled"): void {
    const source = this.cancelTokenMap.get(requestType);
    if (source) {
      source.cancel(message);
      // Remove the source from the map after cancellation to ensure a fresh one is created next time
      this.cancelTokenMap.delete(requestType);
    }
  }

  async postUploadPdf(formData: FormData) {
    const requestType = IRequestType.UPLOAD_PDF;
    const request = await this.post<IProcessPdfResponse>(
      API_ROUTES_CONFIG.upload,
      formData,
      {
        headers: { "Content-Type": "multipart/form-data" },
      },
      requestType,
    ).then((response) => {
      if (typeof response.data === "string") {
        try {
          response.data = JSON.parse(response.data);
        } catch (e) {
          response.data.error = "Cannot process, please retry.";
        }
      }
      return response;
    });

    const cancel = (message?: string) => {
      this.cancel(requestType, message);
    };
    return { request, cancel };
  }
}
