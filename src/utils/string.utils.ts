export function getRandomHexString(length: number = 15): string {
  if (length <= 0) {
    return "";
  }

  let hexString = "";
  const characters = "0123456789abcdef";

  for (let i = 0; i < length; i++) {
    const randomIndex = Math.floor(Math.random() * characters.length);
    hexString += characters[randomIndex];
  }

  return hexString;
}
