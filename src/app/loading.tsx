import { FC } from "react";
import { PageLoader } from "./components/common/Loaders";

const Loading: FC = () => <PageLoader />;

export default Loading;
