import React from "react";
import { SidebarProvider } from "@/app/context/SidebarContext";
import { Header } from "./components/Header";

export default function Index(props: { children: React.ReactNode }): JSX.Element {
  return (
    <SidebarProvider>
      <div className="flex flex-col h-screen dark:bg-gray-900">
        <Header />
        <div className="flex flex-grow overflow-auto bg-gradient-to-t from-slate-100 to-slate-200 dark:from-slate-700 dark:to-slate-800">
          <main className="flex flex-grow w-full">{props.children}</main>
        </div>
      </div>
    </SidebarProvider>
  );
}
