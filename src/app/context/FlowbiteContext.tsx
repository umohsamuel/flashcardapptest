"use client";
import { flowbiteTheme } from "@/app/theme";
import { Flowbite } from "flowbite-react";
import { FC, PropsWithChildren } from "react";

const FlowbiteContext: FC<PropsWithChildren> = function ({ children }) {
  return <Flowbite theme={{ theme: flowbiteTheme }}>{children}</Flowbite>;
};

export default FlowbiteContext;
