"use client";

import { Table } from "flowbite-react";
import { FC, useState } from "react";
import { useRouter } from "next/navigation";
import { ROUTES_CONFIG } from "@/constants/routes.constants";
import { Titles } from "@/app/components/common/Titles";
import { ClassTableRowDeck } from "./ClassTableRowDeck";
import { useSpaceId } from "@/hooks/navigation.hooks";
import { Links } from "@/app/components/common/Links";
import { DeleteClassModal } from "@/app/components/modals/DeleteClassModal";

interface IClassTableProps {
  classItem: IGraphQLClass;
  refetch: () => void;
}

export const ClassTable: FC<IClassTableProps> = (props) => {
  const router = useRouter();
  const spaceId = useSpaceId();
  const [isDeleteClassModalVisible, setIsDeleteClassModalVisible] = useState(false);

  const handleOpenClass = (classId: string) => {
    const classRoute = ROUTES_CONFIG.getClassRoute({
      spaceId,
      classId,
    });
    router.push(classRoute);
  };

  return (
    <>
      <div className="dark:text-white">
        <Titles.SubTitle className="text-center mb-0">
          {props.classItem?.name ?? "<no class name>"}
        </Titles.SubTitle>
        <div className="flex justify-center mb-3 mt-0 gap-4">
          <Links.TableLink className="text-l" onClick={() => handleOpenClass(props.classItem.id)}>
            Show
          </Links.TableLink>
          <Links.TableLink className="text-l" onClick={() => setIsDeleteClassModalVisible(true)}>
            Delete
          </Links.TableLink>
        </div>
        <Table className="max-w-2xl mx-auto mb-10">
          <Table.Head>
            <Table.HeadCell>Decks</Table.HeadCell>
          </Table.Head>
          <Table.Body className="divide-y">
            {props.classItem.decks.length > 0 ? (
              props.classItem.decks.map((deck) => (
                <ClassTableRowDeck key={deck.id} classId={props.classItem.id} deck={deck} />
              ))
            ) : (
              <Table.Row className="bg-white dark:border-gray-700 dark:bg-gray-800">
                <Table.Cell colSpan={2} className="text-center">
                  No decks yet
                </Table.Cell>
              </Table.Row>
            )}
          </Table.Body>
        </Table>
      </div>
      {isDeleteClassModalVisible && (
        <DeleteClassModal
          modalOpen
          setOpenModal={(value: boolean) => setIsDeleteClassModalVisible(value)}
          classIdToDelete={props.classItem.id}
          refetch={() => {
            props.refetch?.();
          }}
        />
      )}
    </>
  );
};
