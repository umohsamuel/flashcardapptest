"use client";

import { Table, Button } from "flowbite-react";
import { FC, useState } from "react";
import { useRouter } from "next/navigation";
import { ROUTES_CONFIG } from "@/constants/routes.constants";
import { DeleteDeckModal } from "@/app/components/modals/DeleteDeckModal";
import { useSpaceId } from "@/hooks/navigation.hooks";

interface IClassTableRowDeckProps {
  classId: string;
  deck: IGraphQLDeck;
  refetch?: () => void;
}

export const ClassTableRowDeck: FC<IClassTableRowDeckProps> = (props) => {
  const router = useRouter();
  const spaceId = useSpaceId();
  const [isDeleteDeckModalVisible, setIsDeleteDeckModalVisible] = useState(false);
  const handleOpenDeck = () => {
    const classRoute = ROUTES_CONFIG.getDeckRoute({
      spaceId: spaceId,
      classId: props.classId,
      deckId: props.deck.id,
    });
    router.push(classRoute);
  };

  const onDeleteDeck = async () => {
    setIsDeleteDeckModalVisible(true);
  };

  return (
    <>
      <Table.Row key={props.deck.id} className="bg-white dark:border-gray-700 dark:bg-gray-800">
        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
          {props.deck.name}
        </Table.Cell>
      </Table.Row>
      {isDeleteDeckModalVisible && (
        <DeleteDeckModal
          modalOpen
          setOpenModal={(value: boolean) => setIsDeleteDeckModalVisible(value)}
          deckIdToDelete={props.deck.id}
          refetch={() => {
            props.refetch?.();
          }}
        />
      )}
    </>
  );
};
