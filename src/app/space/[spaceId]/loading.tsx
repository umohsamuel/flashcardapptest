"use client";

import { FC } from "react";
import { PageLoader } from "@/app/components/common/Loaders";

const Loading: FC = () => <PageLoader />;

export default Loading;
