"use client";

import { useGraphQLSpace } from "@/graphql/domain/user.graphql";
import { PageWrapper } from "@/app/components/PageWrapper";
import { ClassTable } from "./components/ClassTable";
import { TextInput, Label } from "flowbite-react";
import toast from "react-hot-toast";
import { HiClipboardCopy } from "react-icons/hi";

export default function SpacePage() {
  const { data, refetch } = useGraphQLSpace();
  const classes = data?.user?.classes ?? [];

  const pathName = window?.location.href ?? "";

  const copyToClipboard = () => {
    navigator.clipboard.writeText(pathName).then(() => {
      toast.remove();
      toast.success("Link copied!");
    });
  };

  return (
    <PageWrapper title="Classes overview">
      <div className="flex flex-col justify-center items-center">
        <div className="max-w-md cursor-pointer">
          <div className="mb-2 flex justify-center">
            <Label
              htmlFor="shareLink"
              value="Make sure to save this link to keep access to your classes"
            />
          </div>
          <TextInput
            id="shareLink"
            className="w-2xl cursor-pointer"
            placeholder="https://example.com/your/share/link"
            required
            readOnly
            rightIcon={HiClipboardCopy}
            type="text"
            value={pathName}
            onClick={copyToClipboard}
          />
        </div>
      </div>
      <section className="mt-6 flex flex-col overflow-auto container mx-auto">
        {classes.map((classItem) => (
          <ClassTable key={classItem.id} classItem={classItem} refetch={() => refetch()} />
        ))}
      </section>
    </PageWrapper>
  );
}
