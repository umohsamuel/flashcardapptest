"use client";

import React from "react";
import { Flashcard } from "@/app/components/Flashcard";
import { PlayIcon } from "@heroicons/react/24/solid";
import { styled } from "styled-components";
import { Buttons } from "@/app/components/common/Buttons";
import { useStudyMode } from "@/hooks/studyMode.hooks";

const Container = styled.div`
  padding-bottom: 50px;
`;

interface IFlashcardDeckProps {
  deck: IGraphQLDeck;
  classId: string;
}

export const FlashcardDeck: React.FC<IFlashcardDeckProps> = (props) => {
  const { startStudying } = useStudyMode();

  const handleStartStudying = () => {
    const randomIndex = Math.floor(Math.random() * props.deck.flashcards.length);
    const randomFlashcardId = props.deck.flashcards[randomIndex].id;
    startStudying({
      deckId: props.deck.id,
      flashcardId: randomFlashcardId,
      classId: props.classId,
    });
  };

  return (
    <Container className="flex flex-1 flex-col dark:text-white">
      <div className="flex justify-center mb-2">
        <Buttons.PrimaryButton
          onClick={handleStartStudying}
          size="sm"
          IconComponent={<PlayIcon className="h-5 w-5 mr-1" />}
        >
          Start studying
        </Buttons.PrimaryButton>
      </div>
      <div className="flex overflow-y-auto flex-col p-6 items-center">
        {props.deck.flashcards.map((flashcard) => (
          <Flashcard key={flashcard.id} flashcard={flashcard} />
        ))}
      </div>
    </Container>
  );
};
