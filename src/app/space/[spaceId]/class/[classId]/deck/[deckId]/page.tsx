"use client";

import React from "react";
import { useGraphQLDeck } from "@/graphql/domain/deck.graphql";
import { PageWrapper } from "@/app/components/PageWrapper";
import { FlashcardDeck } from "./components/FlashcardDeck";
import { useClassId } from "@/hooks/navigation.hooks";

export default function DeckPage() {
  const result = useGraphQLDeck();
  const deck = result.data?.deck;

  const classId = useClassId();

  return (
    <PageWrapper title={deck?.name}>
      {Boolean(deck) && (
        <section className="flex overflow-auto">
          <FlashcardDeck classId={classId as string} deck={deck!} />
        </section>
      )}
    </PageWrapper>
  );
}
