"use client";

import React from "react";
import { useGraphQLClass, useGraphQLUpdateClass } from "@/graphql/domain/class.graphql";
import { UserDeckListForClass } from "@/app/components/UserDeckListForClass";
import { EditableTitle } from "@/app/components/common/EditableTitle";
import { PageWrapper } from "@/app/components/PageWrapper";

export default function ClassPage() {
  const { data } = useGraphQLClass();
  const { updateClass } = useGraphQLUpdateClass();

  const classObject = data?.clazz;

  const handleSaveClick = async (classNameEdit: string) => {
    if (classNameEdit !== classObject?.name && classObject?.id) {
      await updateClass({ id: classObject.id, name: classNameEdit });
    }
  };

  return (
    <PageWrapper
      title={
        <EditableTitle
          title={classObject?.name}
          onEdit={handleSaveClick}
          className="justify-center items-center"
          classNameEditIcon="ml-4"
        />
      }
    >
      {Boolean(classObject?.id) && (
        <section className="container mx-auto">
          <UserDeckListForClass classId={classObject?.id as string} />
        </section>
      )}
    </PageWrapper>
  );
}
