import React from "react";
import { Toaster } from "react-hot-toast";
import { Inter } from "next/font/google";
import FlowbiteContext from "@/app/context/FlowbiteContext";
import { ApolloWrapper } from "@/graphql/apollo-wrapper";
import { AppContextProvider } from "@/context/AppContext";

import "./../globals.css";
import OurProvider from "@/redux/OurProvider";

const inter = Inter({ subsets: ["latin"] });

const RootLayout = function ({ children }: { children: React.ReactNode }) {
  return (
    <html lang="en" data-mode="dark">
      <head>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta httpEquiv="X-UA-Compatible" content="ie=edge" />

        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
        <link rel="manifest" href="/site.webmanifest" />

        <meta property="og:type" content="website" />
        <meta property="og:title" content="Flash Card Web App" />
        <meta property="og:url" content="Flash Card Web App" />

        <title>Flash Card Web App</title>
      </head>
      <body className={inter.className}>
        <AppContextProvider>
          <OurProvider>
            <FlowbiteContext>
              <Toaster />
              <ApolloWrapper>{children}</ApolloWrapper>
            </FlowbiteContext>
          </OurProvider>
        </AppContextProvider>
      </body>
    </html>
  );
};

export default RootLayout;
