// hello thank for giving me a  chance to participate in this test,i have made some changes throughout the files to optimize SSR and performance

import React from "react";
import { Titles } from "./components/common/Titles";
import SpacesCardList from "./components/SpacesCardList";
import { PageWrapper } from "./components/PageWrapper";
import { FileUploadNewClass } from "./components/fileUpload/FileUploadNewClass";

export default function Page() {
  return (
    <PageWrapper title="Create your flashcards - Powered by AI.">
      <div className="flex justify-center">
        <hr className="mb-10 max-w-2xl w-full border rounded" />
      </div>
      <section className="flex flex-col justify-center items-center">
        <Titles.CardTitle>
          <p>Upload a PDF here to generate flashcards by AI.</p>
        </Titles.CardTitle>
        <FileUploadNewClass />
      </section>
      <Titles.PageTitle className="mt-16 mb-2 text-center ">Existing spaces</Titles.PageTitle>
      <section className="mt-2 flex flex-col mb-5">
        <SpacesCardList />
      </section>
    </PageWrapper>
  );
}
