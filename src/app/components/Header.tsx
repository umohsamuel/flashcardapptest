"use client";

import { Navbar } from "flowbite-react";
import Image from "next/image";
import { FC } from "react";
import { AddNewClassButton } from "./AddNewClassButton";
import { useGraphQLSpace } from "@/graphql/domain/user.graphql";
import { useSpaceId } from "@/hooks/navigation.hooks";

export const Header: FC<Record<string, never>> = function () {
  const { refetch } = useGraphQLSpace();

  const spaceId = useSpaceId();

  // const toggleDarkMode = () => {
  //   const currentMode = document.documentElement.getAttribute("data-mode");
  //   if (!isBrowser()) {
  //     return;
  //   }
  //   if (currentMode === "dark") {
  //     document.documentElement.setAttribute("data-mode", "light");
  //     localStorage.theme = "light";
  //   } else {
  //     document.documentElement.setAttribute("data-mode", "dark");
  //     localStorage.theme = "dark";
  //   }
  // };

  // useEffect(() => {
  //   document.documentElement.setAttribute(
  //     "data-mode",
  //     (isBrowser() && localStorage.theme) ?? "dark"
  //   );
  // }, []);

  return (
    <header className="top-0 z-20 ">
      <Navbar
        fluid
        className="bg-gradient-to-b from-slate-100 to-slate-200 dark:from-slate-700 dark:to-slate-800"
      >
        <Navbar.Brand href="/">
          <Image alt="Flowbite logo" height="36" src="/logo.png" width="36" />
          <span className="self-center whitespace-nowrap px-3 text-xl font-semibold dark:text-white">
            Flash Cards - AI Powered
          </span>
        </Navbar.Brand>
        <div className="flex md:order-2">
          <Navbar.Toggle />
          {spaceId && (
            <div className="mr-2">
              {" "}
              <AddNewClassButton refetch={() => refetch()} />
            </div>
          )}
          {/* <DarkThemeToggle /> */}
        </div>
      </Navbar>
    </header>
  );
};
