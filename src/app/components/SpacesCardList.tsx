"use client";
import React, { useMemo } from "react";
import { useGraphQLUsers, useGraphQLUpdateUser } from "@/graphql/domain/user.graphql";
import { Titles } from "./common/Titles";
import { Button, Card } from "flowbite-react";
import { FaTrash } from "react-icons/fa";
import { styled } from "styled-components";
import { DeleteSpaceModal } from "./modals/DeleteSpaceModal";
import { useRouter } from "next/navigation";
import { ROUTES_CONFIG } from "@/constants/routes.constants";
import { EditableTitle } from "./common/EditableTitle";

const DeleteIconWrapper = styled.div`
  position: absolute;
  top: 0.5em;
  right: 0.5em;
  cursor: pointer;
  padding: 0.5em;
  border-radius: 0.3em;
`;

const SpacesCardList: React.FC = () => {
  const { data } = useGraphQLUsers();
  const { updateUser } = useGraphQLUpdateUser();

  const router = useRouter();

  const users = useMemo(() => {
    const usersRaw = data?.users ?? [];
    return [...usersRaw].sort((a, b) => a.id.localeCompare(b.id));
  }, [data?.users]);

  const [deleteModalVisible, setDeleteModalVisible] = React.useState(false);
  const [userIdToDelete, setUserIdToDelete] = React.useState<string | null>(null);

  const handleSpaceNameUpdate = async (userId: string, newName: string) => {
    try {
      await updateUser({ id: userId, name: newName });
    } catch (err) {
      console.error("Failed to update space name", err);
    }
  };

  return (
    <div className="flex flex-col justify-center">
      <div className="flex justify-center overflow-auto flex-grow">
        <div className="flex flex-wrap justify-center">
          {users.map((user, index) => (
            <Card key={user.id} className="relative m-3 p-0" style={{ width: 400 }}>
              <EditableTitle
                className="truncate"
                classNameText="truncate flex flex-grow"
                title={user.name ? user.name : `Space ${index + 1}`}
                onEdit={(newName: string) => handleSpaceNameUpdate(user.id, newName)}
                TitleComponent={Titles.CardTitle}
                classNameSaveIcon="ml-2 p-0"
                classNameAbortIcon="ml-1 p-0 mr-3"
                classNameEditIcon="ml-2 p-0 mr-3"
              />
              <Button
                onClick={() => router.push(ROUTES_CONFIG.getSpaceRoute({ spaceId: user.id }))}
              >
                Enter Space
              </Button>
              <DeleteIconWrapper
                className="hover:bg-gray-100"
                onClick={() => {
                  setDeleteModalVisible(true);
                  setUserIdToDelete(user.id);
                }}
              >
                <FaTrash />
              </DeleteIconWrapper>
            </Card>
          ))}
        </div>
      </div>

      <DeleteSpaceModal
        modalOpen={deleteModalVisible}
        setOpenModal={setDeleteModalVisible}
        userIdToDelete={userIdToDelete}
      />
    </div>
  );
};

export default SpacesCardList;
