"use client";

import React, { FC, useState } from "react";
import { DeckList } from "./DeckList";
import { NewDeckModal } from "./modals/NewDeckModal";
import { useGraphQLClass } from "@/graphql/domain/class.graphql";
import { Buttons } from "./common/Buttons";
import { CogIcon } from "@heroicons/react/24/solid";

interface IUserDeckListForClassProps {
  classId: string;
}

export const UserDeckListForClass: FC<IUserDeckListForClassProps> = (props) => {
  const [isNewDeckModalVisible, setIsNewDeckModalVisible] = useState(false);

  const { refetch } = useGraphQLClass();

  return (
    <div className="mx-auto max-w-3xl">
      <DeckList />

      <div className="mt-4">
        <Buttons.PrimaryButton
          IconComponent={<CogIcon className="w-5 h-5 mr-1" />}
          onClick={() => setIsNewDeckModalVisible(true)}
        >
          Generate new deck
        </Buttons.PrimaryButton>
      </div>

      {isNewDeckModalVisible && (
        <NewDeckModal
          modalOpen
          setOpenModal={(value: boolean) => setIsNewDeckModalVisible(value)}
          refetch={() => {
            refetch();
          }}
        />
      )}
    </div>
  );
};
