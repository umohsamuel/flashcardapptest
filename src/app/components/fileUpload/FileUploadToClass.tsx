import React, { FC } from "react";
import { usePdfProcessorState } from "@/hooks/pdfProcessor.hooks";
import { Button, Card } from "flowbite-react";
import { AxiosResponse } from "axios";
import { FileUploadForm } from "./FileUploadForm";
import { useDispatch } from "react-redux";
import { useGraphQLClass } from "@/graphql/domain/class.graphql";
import { reduxActions } from "@/redux/actions";
import { EPdfProcessorState } from "@/redux/reducers/pdfProcessor.slice";
import { Titles } from "../common/Titles";
import { FileUploadCommon } from "./FileUplaodCommonComponents";

interface IFileUploadProps {
  classId?: string;
  modalOpen?: boolean;
  setOpenModal?: (state: boolean) => void;
  refetch?: () => void;
}

export const FileUploadToClass: FC<IFileUploadProps> = (props) => {
  const dispatch = useDispatch();
  const { refetch: refetchClass } = useGraphQLClass();
  const { isReady, isProcessing, isCompleted, isError, lastResponse } = usePdfProcessorState();
  const numberDeckUuidsResult = (lastResponse?.deckUUIDs && lastResponse?.deckUUIDs.length) ?? 0;

  const onSuccess = (response?: AxiosResponse<IProcessPdfResponse>) => {
    refetchClass();
    props.refetch?.();
  };

  const onClose = async () => {
    props.setOpenModal?.(false);
    dispatch(
      reduxActions.pdfProcessor.setUploadState({
        uploadState: EPdfProcessorState.ready,
      })
    );
    props.refetch?.();
    window.location.reload();
  };

  const showCard = isProcessing || isCompleted;
  let textChangeClassName = showCard
    ? isProcessing
      ? "Your PDF is processing...."
      : "Your PDF has finished processing."
    : "";

  if (isError) {
    textChangeClassName = "There was an error in parsing the response. Please try again.";
  }

  return (
    <div className="flex flex-column items-center">
      {isReady && <FileUploadForm onSuccess={onSuccess} classId={props.classId} />}
      {showCard && (
        <div className="mt-5 max-w-xl">
          <Card>
            {isProcessing && <FileUploadCommon.Loading />}
            {isError && <FileUploadCommon.Error />}
            <Titles.ParagraphTitle>{textChangeClassName}</Titles.ParagraphTitle>
            {Boolean(numberDeckUuidsResult) && (
              <Titles.ParagraphTitle>
                {numberDeckUuidsResult} {numberDeckUuidsResult! > 1 ? "decks were" : "deck was"}{" "}
                generated
              </Titles.ParagraphTitle>
            )}
            {Boolean(isCompleted) && Boolean(props.setOpenModal) && (
              <Button onClick={onClose}>close</Button>
            )}
          </Card>
        </div>
      )}
    </div>
  );
};
