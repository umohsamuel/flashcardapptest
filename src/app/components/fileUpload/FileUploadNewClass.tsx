"use client";

import React, { FC, useState } from "react";
import { usePdfProcessorState } from "@/hooks/pdfProcessor.hooks";
import { Card } from "flowbite-react";
import { AxiosResponse } from "axios";
import { FileUploadSubmitFormNewClass } from "./FileUploadSubmitFormNewClass";
import { FileUploadForm } from "./FileUploadForm";
import { CheckCircleIcon } from "@heroicons/react/24/outline";
import { IconMessage } from "../common/IconMessage";
import { FileUploadCommon } from "./FileUplaodCommonComponents";
import { Titles } from "../common/Titles";

interface IFileUploadProps {
  onSuccess?: (response?: AxiosResponse<IProcessPdfResponse>) => void;
  closeModal?: () => void;
}

export const FileUploadNewClass: FC<IFileUploadProps> = (props) => {
  const [numberDeckUuidsResult, setNumberDeckUuidsResult] = useState<number | null>(null);

  const { isProcessing, isCompleted, isError, lastResponse } = usePdfProcessorState();

  const onSuccess = (response?: AxiosResponse<IProcessPdfResponse>) => {
    const deckUUIDs = response?.data.deckUUIDs;
    if (deckUUIDs && deckUUIDs.length) {
      setNumberDeckUuidsResult(deckUUIDs.length);
    }
  };

  return (
    <div className="flex flex-col items-center">
      <Card className="flex px-10 py-3">
        <FileUploadForm onSuccess={onSuccess} />
        {(isProcessing || isCompleted) && (
          <div className="mt-10 max-w-xl">
            {isProcessing && (
              <>
                <FileUploadCommon.Loading />
                <Titles.ParagraphTitle>
                  While your PDF is processing, please choose a name for the class.
                </Titles.ParagraphTitle>
              </>
            )}
            {isCompleted && (
              <IconMessage icon={<CheckCircleIcon />}>
                Your PDF has finished processing, please choose a name for the class.
              </IconMessage>
            )}
            {isError && <FileUploadCommon.Error />}
            {Boolean(numberDeckUuidsResult) && (
              <Titles.ParagraphTitle>
                {numberDeckUuidsResult} {numberDeckUuidsResult! > 1 ? "decks were" : "deck was"}{" "}
                generated
              </Titles.ParagraphTitle>
            )}
            <FileUploadSubmitFormNewClass responseState={lastResponse ?? undefined} />
          </div>
        )}
      </Card>
    </div>
  );
};
