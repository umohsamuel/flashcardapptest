import React, { FC, useState } from "react";
import { toast } from "react-hot-toast";
import { useUploadPdf } from "@/hooks/pdfProcessor.hooks";
import { FileInput, Select } from "flowbite-react";
import { AxiosResponse } from "axios";
import { useFormik } from "formik";
import * as Yup from "yup";
import { EPdfUploadVerboseOptions } from "@/constants/api.constants";
import { FormComponents } from "../common/FormComponents";
import { Buttons } from "../common/Buttons";
import { CogIcon } from "@heroicons/react/24/solid";

interface IFileUploadProps {
  classId?: string;
  onSuccess?: (response?: AxiosResponse<IProcessPdfResponse>) => void;
  onError?: (message: string) => void;
  closeModal?: () => void;
  onStartUpload?: () => void;
}

export const FileUploadForm: FC<IFileUploadProps> = (props) => {
  const [file, setFile] = useState<File | null>(null);

  const formik = useFormik({
    initialValues: {
      amount: 5,
      file: null,
      verbose: EPdfUploadVerboseOptions.MEDIUM,
    },
    validationSchema: Yup.object({
      amount: Yup.number()
        .required("Amount is required")
        .positive("Amount should be positive")
        .integer("Amount should be an integer")
        .min(1, "Amount should be at least 1")
        .max(50, "Amount should not exceed 50"),
      file: Yup.mixed().required("A file is required"), // Validate presence of file
    }),
    onSubmit: async (values) => {
      // The file validation is now handled by Yup
      props.onStartUpload?.();

      handleUploadPdf(file!, {
        amount: values.amount,
        verbose: values.verbose,
        onError: (message) => {
          toast.error(message);
          props.onError?.(message);
        },
        onSuccess: (message, response) => {
          toast.success(message);
          props.onSuccess?.(response);
        },
      });
    },
  });

  const handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const files = e.target.files;
    if (files) {
      setFile(files[0]);
      formik.setFieldValue("file", files[0]); // Setting file to formik values
    }
  };

  const { handleUpload: handleUploadPdf } = useUploadPdf(props.classId);

  return (
    <div className="flex flex-col items-center flex-grow">
      <form onSubmit={formik.handleSubmit}>
        <FormComponents.FormGroup>
          <FormComponents.FormLabel>
            Desired amount of flashcards: {formik.values.amount}
          </FormComponents.FormLabel>
          <input
            className="w-full"
            type="range"
            name="amount"
            min="1"
            max="50"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.amount.toString()}
          />
          {formik.touched.amount && formik.errors.amount ? (
            <div className="text-red-500">{formik.errors.amount}</div>
          ) : null}
        </FormComponents.FormGroup>
        <FormComponents.FormGroup>
          <FormComponents.FormLabel htmlFor="verboseSelect" value="Verbosity level" />
          <Select
            id="verboseSelect"
            name="verbose"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.verbose}
          >
            <option value={EPdfUploadVerboseOptions.SHORT}>
              short - flashcards will be short and concise
            </option>
            <option value={EPdfUploadVerboseOptions.MEDIUM}>
              medium - keyword sentence structure
            </option>
            <option value={EPdfUploadVerboseOptions.LONG}>
              medium - full sentences and explanations
            </option>
          </Select>
        </FormComponents.FormGroup>
        <FormComponents.FormGroup>
          <FormComponents.FormLabel htmlFor="verboseSelect" value="PDF File" />
          <FileInput
            id="file"
            className="flex flex-grow w-full"
            accept=".pdf"
            onChange={handleFileChange}
          />
          {formik.touched.file && formik.errors.file ? (
            <div className="text-red-500">{formik.errors.file}</div>
          ) : null}
        </FormComponents.FormGroup>
        <FormComponents.FormGroup className="mb-0">
          <Buttons.PrimaryButton
            type="submit"
            className="w-auto inline-flex"
            IconComponent={<CogIcon className="w-5 h-5 mr-1" />}
          >
            <span>Generate</span>
          </Buttons.PrimaryButton>
        </FormComponents.FormGroup>
      </form>
    </div>
  );
};
