"use client";

import { FC, useEffect, useState } from "react";
import { useGraphQLUserClasses, useGraphQLUpdateClass } from "@/graphql/domain/class.graphql";
import { usePdfProcessorState } from "@/hooks/pdfProcessor.hooks";
import { useRouter } from "next/navigation";
import { ROUTES_CONFIG } from "@/constants/routes.constants";
import { getRandomHexString } from "@/utils/string.utils";
import { useDispatch } from "react-redux";
import { reduxActions } from "@/redux/actions";
import { EPdfProcessorState } from "@/redux/reducers/pdfProcessor.slice";
import { INewClassFormSubmitData, NewClassForm } from "../forms/NewClassForm";

interface ISubmitButtonProps {
  responseState?: IProcessPdfResponse;
}

export const FileUploadSubmitFormNewClass: FC<ISubmitButtonProps> = (props) => {
  console.log("🚀 ~ props:", props);
  const { isProcessing, isCompleted, isAborted, isError } = usePdfProcessorState();
  const router = useRouter();
  const dispatch = useDispatch();

  const [formRenderKey, setFormRenderKey] = useState(getRandomHexString());

  const { updateClass } = useGraphQLUpdateClass();
  const { refetch: refetchClasses } = useGraphQLUserClasses();

  useEffect(() => {
    if (isAborted || isError) {
      setFormRenderKey(getRandomHexString());
    }
  }, [isAborted, isError]);

  const handleSubmit = async ({ name, description }: INewClassFormSubmitData) => {
    if (!isCompleted || !props.responseState) {
      return;
    }
    console.log("🚀 ~ props.responseState:", props.responseState);
    const { classUUID, userUUID, deckUUIDs } = props.responseState!;
    if (!classUUID || !userUUID) {
      return;
    }
    updateClass({ id: classUUID, name, description }).then(() => {
      refetchClasses();
    });
    if (deckUUIDs?.length && deckUUIDs?.length >= 2) {
      router.push(ROUTES_CONFIG.getClassRoute({ spaceId: userUUID, classId: classUUID }));
    }
    if (deckUUIDs?.length && deckUUIDs?.length === 1) {
      router.push(
        ROUTES_CONFIG.getDeckRoute({
          spaceId: userUUID,
          classId: classUUID,
          deckId: deckUUIDs[0],
        })
      );
    }
    dispatch(
      reduxActions.pdfProcessor.setUploadState({
        uploadState: EPdfProcessorState.ready,
      })
    );
  };
  return (
    <NewClassForm
      key={formRenderKey}
      onSubmit={handleSubmit}
      isSubmitDisabled={!isCompleted}
      isSubmitLoading={isProcessing}
    />
  );
};
