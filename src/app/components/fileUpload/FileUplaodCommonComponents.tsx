import React, { FC } from "react";
import { Spinner } from "flowbite-react";
import { RowSpinner } from "../common/Loaders";
import { IconMessage } from "../common/IconMessage";
import { ShieldExclamationIcon } from "@heroicons/react/24/outline";

const Error: FC = () => {
  return (
    <IconMessage icon={<ShieldExclamationIcon />}>
      There was an error in parsing the response. Please try again.
    </IconMessage>
  );
};

const Loading: FC = () => {
  return (
    <RowSpinner>
      <Spinner aria-label="Spinner" className="flex justify-center" />
    </RowSpinner>
  );
};

export const FileUploadCommon = {
  Error,
  Loading,
};
