"use client";
import { FC, PropsWithChildren } from "react";
import { Breadcrumb as BreadcrumbFlowbite } from "flowbite-react";
import { HiHome, HiOutlineCollection } from "react-icons/hi";
import { useSpaceId, useClassId, useDeckId } from "@/hooks/navigation.hooks";
import { useGraphQLSpace } from "@/graphql/domain/user.graphql";
import { useGraphQLClass } from "@/graphql/domain/class.graphql";
import { useGraphQLDeck } from "@/graphql/domain/deck.graphql";
import { ROUTES_CONFIG } from "@/constants/routes.constants";
import { styled } from "styled-components";
import { useRouter } from "next/navigation";
import { FaGraduationCap } from "react-icons/fa";

const BreadcrumbName = styled.p`
  margin-left: 5px;
`;

const BreadcrumbItem: FC<
  PropsWithChildren<{
    onClick: () => void;
  }>
> = ({ onClick, children }) => {
  return (
    <BreadcrumbFlowbite.Item className="cursor-pointer" onClick={onClick}>
      <div className="flex items-center hover:bg-gray-100 dark:text-white p-2 dark:hover:bg-slate-900 rounded">
        {children}
      </div>
    </BreadcrumbFlowbite.Item>
  );
};

export const Breadcrumb: FC = () => {
  const router = useRouter();
  const spaceId = useSpaceId();
  const classId = useClassId();
  const deckId = useDeckId();

  const { data: spaceData } = useGraphQLSpace();
  const { data: classData } = useGraphQLClass();
  const { data: deckData } = useGraphQLDeck();

  const spaceName = spaceData?.user.name;
  const className = classData?.clazz?.name;
  const deckName = deckData?.deck?.name;

  return (
    <BreadcrumbFlowbite aria-label="Dynamic breadcrumb example">
      {spaceId && (
        <BreadcrumbItem onClick={() => router.push(ROUTES_CONFIG.getSpaceRoute({ spaceId }))}>
          <HiHome className="mr-1" />
          <BreadcrumbName>Home</BreadcrumbName>
        </BreadcrumbItem>
      )}
      {className && (
        <BreadcrumbItem
          onClick={() => router.push(ROUTES_CONFIG.getClassRoute({ spaceId, classId }))}
        >
          <FaGraduationCap className="mr-1" />
          <BreadcrumbName>{className}</BreadcrumbName>
        </BreadcrumbItem>
      )}
      {deckName && (
        <BreadcrumbItem
          onClick={() => router.push(ROUTES_CONFIG.getDeckRoute({ spaceId, classId, deckId }))}
        >
          <HiOutlineCollection className="mr-1" />
          <BreadcrumbName>{deckName}</BreadcrumbName>
        </BreadcrumbItem>
      )}
    </BreadcrumbFlowbite>
  );
};
