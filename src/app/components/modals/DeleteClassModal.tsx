import { useGraphQLDeleteClass } from "@/graphql/domain/class.graphql";
import { Button, Modal } from "flowbite-react";
import { FC } from "react";
import { toast } from "react-hot-toast";

interface IDeleteClassModalProps {
  modalOpen: boolean;
  setOpenModal: (state: boolean) => void;
  classIdToDelete: string | null;
  refetch?: () => void;
}

export const DeleteClassModal: FC<IDeleteClassModalProps> = (props) => {
  const { deleteClass } = useGraphQLDeleteClass();
  const handleDeleteClassConfirmed = async () => {
    if (props.classIdToDelete) {
      try {
        await deleteClass(props.classIdToDelete);
        props.setOpenModal(false);
        toast.success("Class successfully deleted!");
        props.refetch?.();
      } catch (error) {
        toast.error("An unexpected error occurred. Please retry.");
        console.error("Error deleting class:", error);
      }
    }
  };

  return (
    <Modal
      className="dark:text-white"
      show={props.modalOpen}
      popup
      onClose={() => props.setOpenModal(false)}
    >
      <Modal.Header className="mb-2">Confirm Deletion</Modal.Header>
      <Modal.Body>Are you sure you want to delete this class?</Modal.Body>
      <Modal.Footer>
        <Button onClick={() => props.setOpenModal(false)}>Cancel</Button>
        <Button variant="danger" onClick={handleDeleteClassConfirmed}>
          Confirm
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
