import { Modal } from "flowbite-react";
import { FC } from "react";
import { useGraphQLClass } from "@/graphql/domain/class.graphql";
import { Titles } from "../common/Titles";
import { FileUploadToClass } from "../fileUpload/FileUploadToClass";

interface INewDeckModalProps {
  classId?: string;
  refetch?: () => void;
  modalOpen: boolean;
  setOpenModal: (state: boolean) => void;
}

export const NewDeckModal: FC<INewDeckModalProps> = (props) => {
  const result = useGraphQLClass(props.classId);
  const classObjectName = result.data?.clazz?.name;
  const classNameString = classObjectName ? ` to "${classObjectName}"` : "";

  return (
    <Modal
      show={props.modalOpen}
      popup
      onClose={() => {
        props.setOpenModal(false);
      }}
    >
      <Modal.Header />
      <Modal.Body>
        <div className="text-center">
          <Titles.CardTitle className="mb-5">
            Choose a PDF to add a new deck{classNameString}
          </Titles.CardTitle>
          <div className="inline-flex">
            <FileUploadToClass
              classId={props.classId}
              refetch={props.refetch}
              modalOpen={props.modalOpen}
              setOpenModal={props.setOpenModal}
            />
          </div>
        </div>
      </Modal.Body>
    </Modal>
  );
};
