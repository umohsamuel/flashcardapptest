"use client";

import { Button, Modal } from "flowbite-react";
import { FC, useCallback, useState } from "react";
import { INewClassFormSubmitData, NewClassForm } from "../forms/NewClassForm";
import { useGraphQLAddClass } from "@/graphql/domain/class.graphql";
import { toast } from "react-hot-toast";
import { useSpaceId } from "@/hooks/navigation.hooks";
import { useGraphQLSpace } from "@/graphql/domain/user.graphql";

interface INewClassModalProps {
  modalOpen: boolean;
  setOpenModal: (state: boolean) => void;
  refetch?: () => void;
}

export const NewClassModal: FC<INewClassModalProps> = (props) => {
  const [isLoading, setIsLoading] = useState(false);
  const { addClass } = useGraphQLAddClass();
  const spaceId = useSpaceId();

  const handleSubmit = useCallback(
    async (data: INewClassFormSubmitData) => {
      setIsLoading(true);
      try {
        const result = await addClass({
          userId: spaceId!,
          name: data.name,
          description: data.description,
        });
        if (result.errors) {
          toast.error("Error adding class, please try again.");
        } else {
          setIsLoading(false);
          props.setOpenModal(false);
          props.refetch?.();
        }
      } catch (error) {
        setIsLoading(false);
        // Handle the error as needed.
        console.error("Error adding class:", error);
      }
    },
    [props, addClass, spaceId]
  );

  return (
    <Modal show={props.modalOpen} popup onClose={() => props.setOpenModal(false)}>
      <Modal.Header className="mb-2">Add Class</Modal.Header>
      <Modal.Body>
        <NewClassForm
          key={props.modalOpen ? "modalOpen" : "modalClosed"}
          onSubmit={handleSubmit}
          isSubmitLoading={isLoading}
        />
      </Modal.Body>
    </Modal>
  );
};
