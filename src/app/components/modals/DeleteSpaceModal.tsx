import { Button, Modal } from "flowbite-react";
import { FC } from "react";
import { toast } from "react-hot-toast";
import { useGraphQLDeleteUser, useGraphQLUsers } from "@/graphql/domain/user.graphql";

interface IDeleteSpaceModalProps {
  modalOpen: boolean;
  setOpenModal: (state: boolean) => void;
  userIdToDelete: string | null;
}

export const DeleteSpaceModal: FC<IDeleteSpaceModalProps> = (props) => {
  const { deleteUser } = useGraphQLDeleteUser();
  const { refetch } = useGraphQLUsers();

  const handleDeleteUser = async () => {
    if (props.userIdToDelete) {
      try {
        const result = await deleteUser(props.userIdToDelete);
        if (result.errors) {
          toast.error("Error deleting user/space, please try again.");
        } else {
          props.setOpenModal(false);
          toast.success("User/Space successfully deleted!");
          refetch();
        }
      } catch (error) {
        toast.error("An unexpected error occurred. Please retry");
        console.error("Error deleting user:", error);
      }
    }
  };

  return (
    <Modal
      className="dark:text-white"
      show={props.modalOpen}
      popup
      onClose={() => props.setOpenModal(false)}
    >
      <Modal.Header className="mb-2">Confirm Deletion</Modal.Header>
      <Modal.Body>Are you sure you want to delete this space/user?</Modal.Body>
      <Modal.Footer>
        <Button onClick={() => props.setOpenModal(false)}>Cancel</Button>
        <Button variant="danger" onClick={handleDeleteUser}>
          Confirm
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
