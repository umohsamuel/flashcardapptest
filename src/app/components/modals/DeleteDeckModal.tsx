import { useGraphQLDeleteDeck } from "@/graphql/domain/deck.graphql";
import { Button, Modal } from "flowbite-react";
import { FC } from "react";
import { toast } from "react-hot-toast";

interface IDeleteDeckModalProps {
  modalOpen: boolean;
  setOpenModal: (state: boolean) => void;
  deckIdToDelete: string | null;
  refetch?: () => void;
}

export const DeleteDeckModal: FC<IDeleteDeckModalProps> = (props) => {
  const { deleteDeck } = useGraphQLDeleteDeck();
  const handleDeleteDeckConfirmed = async () => {
    if (props.deckIdToDelete) {
      try {
        await deleteDeck(props.deckIdToDelete);
        props.setOpenModal(false);
        toast.success("Deck successfully deleted!");
        props.refetch?.();
      } catch (error) {
        toast.error("An unexpected error occurred. Please retry");
        console.error("Error deleting deck:", error);
      }
    }
  };

  return (
    <Modal
      className="dark:text-white"
      show={props.modalOpen}
      popup
      onClose={() => props.setOpenModal(false)}
    >
      <Modal.Header className="mb-2">Confirm Deletion</Modal.Header>
      <Modal.Body>Are you sure you want to delete this deck?</Modal.Body>
      <Modal.Footer>
        <Button onClick={() => props.setOpenModal(false)}>Cancel</Button>
        <Button variant="danger" onClick={handleDeleteDeckConfirmed}>
          Confirm
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
