import { Sidebar } from "flowbite-react";
import { SidebarItemProps } from "flowbite-react/lib/esm/components/Sidebar/SidebarItem";
import React, { FC, PropsWithChildren } from "react";
import { styled } from "styled-components";

const StyledSidebarItem = styled(Sidebar.Item)`
  cursor: pointer;
  > span {
    white-space: nowrap;
    max-width: 100%;
    overflow: hidden;
    text-overflow: ellipsis;
  }
`;

export const SidebarItem: FC<PropsWithChildren<SidebarItemProps>> = ({ icon, ...props }) => {
  const cl = props.className ?? "";
  return (
    <StyledSidebarItem
      className={`${cl} text-truncate overflow-hidden cursor-pointer hover:bg-gray-200`}
      {...props}
    >
      {icon ? (
        <div className="flex items-center">
          <div className="flex justify-center items-center h-5 w-5 mr-2">
            {React.createElement(icon)}
          </div>
          <div>{props.children}</div>
        </div>
      ) : (
        props.children
      )}
    </StyledSidebarItem>
  );
};
