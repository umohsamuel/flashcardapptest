import React, { FC, PropsWithChildren } from "react";
import { Label, LabelProps } from "flowbite-react";

interface IFormLabelProps extends LabelProps {}

const FormLabel: FC<PropsWithChildren<IFormLabelProps>> = (props) => {
  return (
    <div className="mb-2 block">
      <Label {...props}>{props.children}</Label>
    </div>
  );
};

interface IFormGroupProps extends React.HTMLAttributes<HTMLDivElement> {}

const FormGroup: FC<PropsWithChildren<IFormGroupProps>> = ({ className, ...props }) => {
  return (
    <div className={`${className ?? ""} flex flex-col justify-center items-center mb-5`} {...props}>
      {props.children}
    </div>
  );
};

export const FormComponents = {
  FormLabel,
  FormGroup,
};
