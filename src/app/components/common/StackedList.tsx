import React, { ReactNode } from "react";

export interface IStackedListElement {
  title?: ReactNode;
  subTitle?: ReactNode;
  mainInformation?: ReactNode;
  secondaryInformation?: ReactNode;
}

const StackedListElement: React.FC<IStackedListElement> = (props) => {
  const isTitleString = typeof props.title === "string";
  const isSubTitleString = typeof props.subTitle === "string";
  const isMainInformationString = typeof props.mainInformation === "string";
  const isSecondaryInformationString = typeof props.secondaryInformation === "string";

  return (
    <li className="flex flex-col sm:flex-row justify-between gap-x-6 gap-y-2 py-5 items-center">
      <div className="flex min-w-0 gap-x-4">
        <div className="min-w-0 flex-auto sm:flex-none">
          {isTitleString ? (
            <p className="text-sm font-semibold leading-6 text-gray-900 dark:text-white">
              {props.title}
            </p>
          ) : (
            props.title
          )}
          {isSubTitleString ? (
            <p className="mt-1 truncate text-xs leading-5 text-gray-500 dark:text-gray-300">
              {props.subTitle}
            </p>
          ) : (
            props.subTitle
          )}
        </div>
      </div>
      <div className="flex flex-col items-start sm:items-end justify-center sm:shrink-0">
        {isMainInformationString ? (
          <p className="text-sm leading-6 text-gray-900">{props.mainInformation}</p>
        ) : (
          props.mainInformation
        )}
        {isSecondaryInformationString ? (
          <p className="mt-1 text-xs leading-5 text-gray-500">{props.secondaryInformation}</p>
        ) : (
          props.secondaryInformation
        )}
      </div>
    </li>
  );
};

interface IStackedListProps {
  listElements: IStackedListElement[];
}

export const StackedLIst: React.FC<IStackedListProps> = (props) => {
  return (
    <ul role="list" className="divide-y divide-gray-400">
      {props.listElements.map((listElement, index) => (
        <div className="px-2" key={index}>
          <StackedListElement {...listElement} />
        </div>
      ))}
    </ul>
  );
};
