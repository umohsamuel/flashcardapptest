import React, { PropsWithChildren } from "react";

type LinkProps = React.DetailedHTMLProps<
  React.AnchorHTMLAttributes<HTMLAnchorElement>,
  HTMLAnchorElement
>;

const TableLink: React.FC<PropsWithChildren<LinkProps>> = ({ children, className, ...props }) => {
  return (
    <a
      className={`font-medium text-cyan-600 hover:underline dark:text-cyan-500 cursor-pointer ${
        className ?? ""
      }`}
      {...props}
    >
      <p>{children}</p>
    </a>
  );
};

export const Links = {
  TableLink,
};
