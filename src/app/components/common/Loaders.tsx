"use client";
import { Spinner } from "flowbite-react";
import { FC, HTMLAttributes } from "react";
import { styled } from "styled-components";

const RowSpinnerWrapper = styled.div`
  > span {
    display: flex;
    justify-content: center;
  }
`;

export const RowSpinner: FC<HTMLAttributes<HTMLDivElement>> = (props) => {
  return (
    <RowSpinnerWrapper {...props}>
      <Spinner aria-label="Spinner" className="flex justify-center" />
    </RowSpinnerWrapper>
  );
};

export const PageLoader: FC = () => (
  <div className="flex flex-grow justify-center items-center w-full h-full dark:text-white">
    <div className="flex gap-2">
      <RowSpinner />
      <span>loading...</span>
    </div>
  </div>
);
