import React, { FC, PropsWithChildren } from "react";
import { Button, ButtonProps as FlowbiteButtonProps } from "flowbite-react";

interface IButtonProps extends FlowbiteButtonProps {
  IconComponent?: React.ReactNode;
}

const PrimaryButton: FC<PropsWithChildren<IButtonProps>> = ({
  className,
  IconComponent,
  ...props
}) => {
  return (
    <Button className={`whitespace-nowrap ${className ?? ""}`} variant="primary" {...props}>
      {IconComponent}
      <span>{props.children}</span>
    </Button>
  );
};

export const Buttons = {
  PrimaryButton,
};
