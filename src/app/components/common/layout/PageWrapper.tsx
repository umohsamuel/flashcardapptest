import React, { FC, PropsWithChildren } from "react";
import { Titles } from "../Titles";
import { Breadcrumb } from "../../BreadCrumb";

interface IPageWrapperProps {
  title: React.ReactNode;
}

export const PageWrapper: FC<PropsWithChildren<IPageWrapperProps>> = (props) => {
  return (
    <div className="flex flex-col w-full overflow-auto">
      <div className="ml-2 md:ml-10 mt-2">
        <Breadcrumb />
      </div>
      <section className="mt-6">
        <div className="text-center">
          <Titles.PageTitle>{props.title}</Titles.PageTitle>
        </div>
      </section>
      {props.children}
    </div>
  );
};
