import React, { FC, PropsWithChildren } from "react";

export const PageFirstSection: FC<PropsWithChildren> = (props) => (
  <section className="flex flex-col justify-center items-center">{props.children}</section>
);

export const PageContainerWide: FC<PropsWithChildren> = (props) => (
  <div className="flex container flex-col">{props.children}</div>
);
