import React, { PropsWithChildren } from "react";

type TitleProps = React.DetailedHTMLProps<
  React.HTMLAttributes<HTMLHeadingElement>,
  HTMLHeadingElement
>;

const PageTitle: React.FC<PropsWithChildren<TitleProps>> = ({ children, className, ...props }) => {
  return (
    <h5
      className={`${className ?? ""} mb-6 text-center text-4xl font-extrabold dark:text-white`}
      {...props}
    >
      {children}
    </h5>
  );
};

const SubTitle: React.FC<PropsWithChildren<TitleProps>> = ({ children, className, ...props }) => {
  return (
    <h5 className={`${className ?? ""} mb-4 text-2xl font-extrabold dark:text-white`} {...props}>
      {children}
    </h5>
  );
};
const CardTitle: React.FC<PropsWithChildren<TitleProps>> = ({ children, className, ...props }) => {
  return (
    <h3
      className={`${className ?? ""} mb-3 text-center text-xl font-bold dark:text-gray-200`}
      {...props}
    >
      {children}
    </h3>
  );
};

const ParagraphTitle: React.FC<PropsWithChildren<TitleProps>> = ({
  children,
  className,
  ...props
}) => {
  return (
    <h5
      className={`${
        className ?? ""
      } mb-1 text-l text-center font-semibold tracking-tight text-gray-900 dark:text-white`}
      {...props}
    >
      {children}
    </h5>
  );
};

export const Titles = {
  PageTitle,
  SubTitle,
  CardTitle,
  ParagraphTitle,
};
