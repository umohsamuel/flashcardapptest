"use client"

import React, { useEffect, useRef, useState } from "react";
import { Button } from "flowbite-react";
import { CheckIcon } from "@heroicons/react/24/solid";
import { BiPencil } from "react-icons/bi";
import { styled } from "styled-components";
import { Titles } from "@/app/components/common/Titles";

type EditableTitleProps = {
  title: string | undefined;
  className?: string;
  iconSize?: number;
  CheckIconComponent?: React.ReactNode;
  DeleteIconComponent?: React.ReactNode;
  EditIconComponent?: React.ReactNode;
  classNameText?: string;
  classNameSaveIcon?: string;
  classNameAbortIcon?: string;
  hideEditIcon?: boolean;
  isEditing?: boolean;
  classNameEditIcon?: string;
  onEdit?: (newTitle: string) => void;
  onAbort?: () => void;
  TitleComponent?: React.ComponentType<React.HTMLAttributes<HTMLElement>>;
};

const IconButton = styled(Button)`
  width: 50px;
`;

const TitleComponentDefault = styled(Titles.PageTitle)`
  margin-bottom: 0 !important;
`;

export const EditableTitle: React.FC<EditableTitleProps> = (props) => {
  const { iconSize = 5 } = props;
  const {
    CheckIconComponent = <CheckIcon className={`h-${iconSize} w-${iconSize} inline-block`} />,
    DeleteIconComponent = "X",
    EditIconComponent = <BiPencil className={`h-${iconSize} w-${iconSize} inline-block`} />,
    TitleComponent = TitleComponentDefault,
    classNameText = "",
    classNameSaveIcon = "ml-2",
    classNameAbortIcon = "ml-2",
    classNameEditIcon = "ml-2 p-0",
  } = props;
  const [isEditing, setIsEditing] = useState(false);
  const [titleEdit, setTitleEdit] = useState(props.title);
  const editRef = useRef<HTMLSpanElement>(null);

  useEffect(() => {
    if (props.isEditing !== undefined && props.isEditing !== isEditing) {
      if (props.isEditing) {
        handleEditClick();
      } else {
        setIsEditing(false);
        clearSelection();
      }
    }
  }, [props.isEditing, isEditing]);

  const handleEditClick = () => {
    setIsEditing(true);
    setTimeout(() => {
      const el = editRef.current;
      if (el) {
        const range = document.createRange();
        const sel = window.getSelection();
        range.setStart(el, 0);
        range.setEnd(el, el.childNodes.length);
        sel?.removeAllRanges();
        sel?.addRange(range);
      }
    }, 10);
  };

  const clearSelection = () => {
    window?.getSelection()?.removeAllRanges();
  };

  const handleSaveClick = () => {
    if (titleEdit) {
      props.onEdit?.(titleEdit);
    } else {
      setTitleEdit(props.title);
    }
    setIsEditing(false);
    clearSelection();
  };

  const handleAbortClick = () => {
    setTitleEdit(props.title);
    setIsEditing(false);
    props.onAbort?.();
  };

  return (
    <div className={`${props.className ?? ""} flex`}>
      <TitleComponent className="mb-0">
        {isEditing ? (
          <span
            ref={editRef}
            className={`ml-1 ${classNameText ?? ""}`}
            contentEditable={true}
            onBlur={(e) => setTitleEdit(e.target.innerText)}
            onKeyDown={(e) => {
              if (e.key === "Enter") {
                e.preventDefault();
                handleSaveClick();
              }
            }}
          >
            {titleEdit}
          </span>
        ) : (
          titleEdit
        )}
      </TitleComponent>
      {isEditing ? (
        <>
          <IconButton className={classNameSaveIcon} color="light" onClick={handleSaveClick}>
            {CheckIconComponent}
          </IconButton>
          <IconButton className={classNameAbortIcon} color="light" onClick={handleAbortClick}>
            {DeleteIconComponent}
          </IconButton>
        </>
      ) : (
        !props.hideEditIcon && (
          <IconButton className={classNameEditIcon} color="light" onClick={handleEditClick}>
            {EditIconComponent}
          </IconButton>
        )
      )}
    </div>
  );
};
