import React, { FC, PropsWithChildren } from "react";
import { Titles } from "./Titles";

interface IIconMessageProps {
  icon: React.ReactElement;
  component?: React.ReactNode;
}

export const IconMessage: FC<PropsWithChildren<IIconMessageProps>> = (props) => {
  return (
    <>
      <div className="flex justify-center max-h-40">
        {React.cloneElement(props.icon, {
          className: "stroke-teal-600",
        })}
      </div>
      <Titles.ParagraphTitle>{props.children}</Titles.ParagraphTitle>
    </>
  );
};
