"use client";

import React, { FC, useState } from "react";
import { PlayIcon } from "@heroicons/react/24/solid";
import { useRouter } from "next/navigation";
import { useGetDeckRoute } from "@/hooks/navigation.hooks";
import { DeleteDeckModal } from "./modals/DeleteDeckModal";
import { Links } from "./common/Links";
import { Buttons } from "./common/Buttons";
import { useStudyMode } from "@/hooks/studyMode.hooks";

interface IDeckListElementProps {
  deck: IGraphQLDeck;
  classId?: string;
  refetch?: () => void;
  onEdit?: () => void;
}

export const DeckListActions: FC<IDeckListElementProps> = (props) => {
  const router = useRouter();
  const getDeckRoute = useGetDeckRoute();
  const [isDeleteDeckModalVisible, setIsDeleteDeckModalVisible] = useState(false);

  const { startStudying } = useStudyMode();

  const handleShow = () => {
    const deckRoute = getDeckRoute(props.deck.id);
    router.push(deckRoute);
  };

  const onDeleteDeck = async () => {
    setIsDeleteDeckModalVisible(true);
  };

  const handleStartStudying = () => {
    const randomIndex = Math.floor(Math.random() * props.deck.flashcards.length);
    const randomFlashcardId = props.deck.flashcards[randomIndex].id;
    startStudying({
      deckId: props.deck.id,
      flashcardId: randomFlashcardId,
      classId: props.classId,
    });
  };

  return (
    <>
      <div className="flex flex-col gap-2">
        <div className="flex gap-5 text-sm">
          <Links.TableLink onClick={handleShow}>
            {/* <Tooltip content="Show">
            <EyeIcon className="h-5 w-5 mr-1 inline-block" />
          </Tooltip> */}
            <span>Manage</span>
          </Links.TableLink>
          <Links.TableLink onClick={onDeleteDeck}>
            {/* <Tooltip content="Delete">
            <TrashIcon className="h-5 w-5 mr-1 inline-block" />
          </Tooltip> */}
            <span>Delete</span>
          </Links.TableLink>
          <Links.TableLink onClick={() => props.onEdit?.()}>
            {/* <Tooltip content="Edit">
            <PencilIcon className="h-5 w-5 mr-1 inline-block" />
          </Tooltip> */}
            <span>Rename</span>
          </Links.TableLink>
        </div>
        <div className="flex flex-grow justify-center">
          <Buttons.PrimaryButton
            size="xs"
            IconComponent={<PlayIcon className="h-3 w-3 mr-1 inline-block" />}
            onClick={handleStartStudying}
          >
            Start studying
          </Buttons.PrimaryButton>
        </div>
      </div>
      {isDeleteDeckModalVisible && (
        <DeleteDeckModal
          modalOpen
          setOpenModal={(value: boolean) => setIsDeleteDeckModalVisible(value)}
          deckIdToDelete={props.deck.id}
          refetch={() => {
            props.refetch?.();
          }}
        />
      )}
    </>
  );
};
