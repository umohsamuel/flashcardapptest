// there is no funtionality to delete, edit or copy the flashcards and also have it updated in the study

import React from "react";
import { DocumentTextIcon, PencilIcon, TrashIcon } from "@heroicons/react/24/solid";
import { Card } from "flowbite-react";

export const Flashcard: React.FC<{ flashcard: IGraphQLFlashcard }> = ({ flashcard }) => {
  return (
    <Card className="relative max-w-sm rounded-xl p-6 bg-white dark:bg-gray-800 w-full mb-4">
      <div className="absolute top-2 right-2 flex space-x-2">
        <DocumentTextIcon className="h-5 w-5 text-gray-500 cursor-pointer" />
        <PencilIcon className="h-5 w-5 text-gray-500 cursor-pointer" />
        <TrashIcon className="h-5 w-5 text-red-500 cursor-pointer" />
      </div>
      <div className="flex flex-col justify-center items-center">
        <div className="w-full flex-grow-0">
          <h2 className="font-semibold text-xl text-center">{flashcard.question}</h2>
          <hr className="border-dashed border my-4 w-full" />
        </div>
        <div className="flex flex-grow flex-shrink h-32 justify-center items-center">
          {flashcard.answer}
        </div>
      </div>
    </Card>
  );
};
