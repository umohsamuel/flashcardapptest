import React, { FC, PropsWithChildren } from "react";
import { Titles } from "./common/Titles";
import { Breadcrumb } from "./BreadCrumb";

interface IPageWrapperProps {
  title: React.ReactNode;
}

export const PageWrapper: FC<PropsWithChildren<IPageWrapperProps>> = (props) => {
  return (
    <div className="flex flex-col w-full overflow-auto">
      <div className="ml-2 sm:ml-16 mt-6">
        <Breadcrumb />
      </div>
      <div className="text-center mt-3">
        <Titles.PageTitle>{props.title}</Titles.PageTitle>
      </div>
      {props.children}
    </div>
  );
};
