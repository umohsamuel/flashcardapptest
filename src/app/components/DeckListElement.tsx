"use client";

import React, { FC, useState } from "react";
import { Button } from "flowbite-react";
import ButtonGroup from "flowbite-react/lib/esm/components/Button/ButtonGroup";
import { EyeIcon, TrashIcon } from "@heroicons/react/24/solid";
import { useRouter } from "next/navigation";
import { useGetDeckRoute } from "@/hooks/navigation.hooks";
import { EditableTitle } from "./common/EditableTitle";
import { styled } from "styled-components";
import { useGraphQLUpdateDeck } from "@/graphql/domain/deck.graphql";

const TitleSpan = styled.span`
  align-items: center;
`;

interface IDeckListProps {
  deck: IGraphQLDeck;
  classId?: string;
  refetch?: () => void;
}

export const DeckListElement: FC<IDeckListProps> = (props) => {
  const router = useRouter();
  const getDeckRoute = useGetDeckRoute();

  const { updateDeck } = useGraphQLUpdateDeck();
  const [isDeleteDeckModalVisible, setIsDeleteDeckModalVisible] = useState(false);

  const handleShow = () => {
    const deckRoute = getDeckRoute(props.deck.id);
    router.push(deckRoute);
  };

  const onDeleteDeck = async () => {
    setIsDeleteDeckModalVisible(true);
  };

  const handleDeckNameUpdate = async (deckId: string, newName: string) => {
    try {
      await updateDeck({ id: deckId, name: newName });
    } catch (err) {
      console.error("Failed to update deck name", err);
    }
  };

  return (
    <li className="flex justify-between items-center py-2">
      <EditableTitle
        TitleComponent={TitleSpan}
        title={props.deck.name}
        className="truncate text-gray-700 dark:text-gray-300 align-center"
        onEdit={(deckName: string) => {
          handleDeckNameUpdate(props.deck.id, deckName);
        }}
        classNameText="truncate flex flex-grow"
      />
      <ButtonGroup>
        <Button variant="link" size="sm" onClick={() => handleShow()}>
          <EyeIcon className="h-5 w-5 mr-2 inline-block" />
          Show
        </Button>
        <Button variant="link" size="sm" onClick={() => onDeleteDeck()}>
          <TrashIcon className="h-5 w-5 mr-2 inline-block" />
          Delete
        </Button>
      </ButtonGroup>
      Y
    </li>
  );
};
