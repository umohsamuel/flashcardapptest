import { FC, useState } from "react";
import { NewClassModal } from "./modals/NewClassModal";
import { Buttons } from "./common/Buttons";
import { PlusIcon } from "@heroicons/react/24/solid";

interface IAddNewClassButtonProps {
  refetch?: () => void;
}

export const AddNewClassButton: FC<IAddNewClassButtonProps> = (props) => {
  const [isNewClassModalOpen, setIsNewClassModalOpen] = useState(false);

  return (
    <>
      <Buttons.PrimaryButton
        IconComponent={<PlusIcon className="w-4 h-4 mr-2" />}
        onClick={() => setIsNewClassModalOpen(true)}
      >
        Add New Class
      </Buttons.PrimaryButton>
      <NewClassModal
        modalOpen={isNewClassModalOpen}
        setOpenModal={(value: boolean) => setIsNewClassModalOpen(value)}
        refetch={props.refetch}
      />
    </>
  );
};
