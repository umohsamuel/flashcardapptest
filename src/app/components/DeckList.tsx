import React, { FC, useMemo, useState } from "react";
import { Titles } from "./common/Titles";
import { useGraphQLClass } from "@/graphql/domain/class.graphql";
import { IStackedListElement, StackedLIst } from "./common/StackedList";
import { DeckListActions } from "./DeckListElementActions";
import { EditableTitle } from "./common/EditableTitle";
import { useGraphQLUpdateDeck } from "@/graphql/domain/deck.graphql";
import styled from "styled-components";

interface IDeckListProps {
  classId?: string;
}

const EditableTitleComponent = styled(Titles.ParagraphTitle)`
  display: flex;
  align-items: center;
`;

export const DeckList: FC<IDeckListProps> = (props) => {
  const { data, refetch } = useGraphQLClass(props.classId);
  const decks = data?.clazz?.decks ?? [];
  const [isEditingDeckId, setIsEditinDeckId] = useState<string | null>(null);

  const { updateDeck } = useGraphQLUpdateDeck();

  const handleDeckNameUpdate = async (deckId: string, name: string) => {
    try {
      await updateDeck({ id: deckId, name: name });
    } catch (err) {
      console.error("Failed to update deck name", err);
    }
  };

  const listElements: IStackedListElement[] = useMemo(
    () =>
      decks.map((deck) => ({
        title: (
          <EditableTitle
            title={deck.name}
            TitleComponent={EditableTitleComponent}
            hideEditIcon
            onAbort={() => {
              setIsEditinDeckId(null);
            }}
            onEdit={(name) => {
              if (deck.name !== name) {
                handleDeckNameUpdate(deck.id, name);
              }
              setIsEditinDeckId(null);
            }}
            isEditing={isEditingDeckId === deck.id}
            classNameSaveIcon=" ml-2"
            classNameAbortIcon=" ml-2"
            classNameEditIcon=" ml-2 p-0"
          />
        ),
        subTitle: deck.flashcards?.length
          ? `${deck.flashcards?.length} flashcard${deck.flashcards.length === 1 ? "" : "s"}`
          : "No flashcards yet",
        mainInformation: <DeckListActions deck={deck} onEdit={() => setIsEditinDeckId(deck.id)} />,
        // secondaryInformation: "",
      })),
    [decks, isEditingDeckId]
  );

  return (
    <>
      {listElements.length > 0 ? (
        <StackedLIst listElements={listElements} />
      ) : (
        <Titles.ParagraphTitle className="text-center">No decks yet</Titles.ParagraphTitle>
      )}
    </>
  );
};
