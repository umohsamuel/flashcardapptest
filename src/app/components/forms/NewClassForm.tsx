import { FC, useState } from "react";
import { Label, Spinner, TextInput, Textarea } from "flowbite-react";
import { Buttons } from "../common/Buttons";
import { CheckCircleIcon } from "@heroicons/react/24/solid";

export interface INewClassFormSubmitData {
  name: string;
  description?: string;
}

interface ISubmitButtonProps {
  responseState?: IProcessPdfResponse;
  onSubmit: (data: INewClassFormSubmitData) => void;
  isSubmitDisabled?: boolean;
  isSubmitLoading?: boolean;
}

export const NewClassForm: FC<ISubmitButtonProps> = (props) => {
  const [classNameValue, setClassNameValue] = useState<string>("");
  const [descriptionValue, setDescriptionValue] = useState<string>("");
  const [classNameInputColor, setClassNameInputColor] = useState<string>();

  const validateForm = () => {
    let valid = true;
    if (!classNameValue.trim()) {
      setClassNameInputColor("failure");
      valid = false;
    }

    return valid;
  };

  const handleSubmit = async () => {
    if (!validateForm()) {
      return;
    }
    props.onSubmit({ name: classNameValue, description: descriptionValue });
  };

  const isClassNameError = classNameInputColor === "failure";

  return (
    <form className="flex flex-col gap-4">
      <div className="flex justify-between">
        <div className="w-full">
          <div className="flex flex-col mb-2">
            <div className="mb-2 block">
              <Label htmlFor="small" value="Class name" color={classNameInputColor} />
            </div>
            <TextInput
              className="flex flex-grow h-full p-0"
              sizing="sm"
              color={classNameInputColor}
              {...(isClassNameError ? { helperText: "Cannot be empty." } : {})}
              value={classNameValue}
              type="text"
              required
              onChange={(event) => setClassNameValue(event.target.value)}
            />
          </div>
          <div className="flex flex-col">
            <div className="mb-2 block">
              <Label htmlFor="large" value="Description (optional)" />
            </div>
            <Textarea
              id="large"
              value={descriptionValue}
              onChange={(event) => setDescriptionValue(event.target.value)}
            />
          </div>
          <div className="flex justify-center mt-3">
            <Buttons.PrimaryButton
              className="w-auto inline-flex"
              onClick={handleSubmit}
              disabled={props.isSubmitDisabled === true}
              IconComponent={!props.isSubmitLoading && <CheckCircleIcon className="w-5 h-5 mr-1" />}
            >
              {props.isSubmitLoading ? (
                <Spinner aria-label="Spinner" className="flex justify-center" />
              ) : (
                <span>Submit</span>
              )}
            </Buttons.PrimaryButton>
          </div>
        </div>
      </div>
    </form>
  );
};
