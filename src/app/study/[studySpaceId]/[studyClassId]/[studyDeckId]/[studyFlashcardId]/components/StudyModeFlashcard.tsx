"use client";

import React, { useState } from "react";
import { Card, Rating } from "flowbite-react";
import { Titles } from "@/app/components/common/Titles";
import { Buttons } from "@/app/components/common/Buttons";
import { useStudyMode } from "@/hooks/studyMode.hooks";

interface IStudyModeFlashcardProps {
  flashcard: IGraphQLFlashcard;
  deck: IGraphQLDeck;
}

export const StudyModeFlashcard: React.FC<IStudyModeFlashcardProps> = (props) => {
  const [isRevealed, setIsRevealed] = useState(false);
  const [rating, setRating] = useState(0);
  const [hoverRating, setHoverRating] = useState(0);

  const { startStudying } = useStudyMode();

  const handleRatingChange = (newRating: number) => {
    setRating(newRating);
    const randomIndex = Math.floor(Math.random() * props.deck.flashcards.length);
    const randomFlashcardId = props.deck.flashcards[randomIndex].id;
    if (randomFlashcardId === props.flashcard.id) {
      setIsRevealed(false);
    } else {
      startStudying({ deckId: props.deck.id, flashcardId: randomFlashcardId });
    }
    setRating(0);
    setHoverRating(0);
  };

  return (
    <div>
      {!isRevealed && (
        <>
          <Titles.SubTitle className="text-center mt-2">Question</Titles.SubTitle>
          <Card className="relative max-w-xl rounded-xl p-6 bg-white dark:bg-gray-800 w-full mb-4">
            <div className="flex flex-col justify-center items-center">
              <Titles.CardTitle className="mt-2">{props.flashcard.question}</Titles.CardTitle>
            </div>
          </Card>
        </>
      )}
      {isRevealed && (
        <div>
          <Titles.SubTitle className="text-center mt-2">Answer</Titles.SubTitle>
          <Card className="relative max-w-xl rounded-xl p-6 bg-white dark:bg-gray-800 w-full mb-4">
            <div className="flex flex-col">
              <Titles.CardTitle>{props.flashcard.answer}</Titles.CardTitle>
            </div>
          </Card>
          <div className="flex flex-col dark:text-white justify-center">
            <Titles.SubTitle className="text-center mt-2">
              How well did you know this card?
            </Titles.SubTitle>
            <div className="flex gap-5  justify-center items-center">
              <span className="hidden sm:flex">Not at all</span>
              <Rating size="lg">
                {[1, 2, 3, 4, 5].map((star) => (
                  <Rating.Star
                    key={star}
                    className="cursor-pointer"
                    filled={star <= (hoverRating || rating)}
                    onClick={() => handleRatingChange(star)}
                    onMouseEnter={() => setHoverRating(star)}
                    onMouseLeave={() => setHoverRating(0)}
                  />
                ))}
              </Rating>
              <span className="hidden sm:flex">Perfect</span>
            </div>
            <span className="absolute bottom-1 right-2 flex text-center text-xs mt-4 justify-center items-center">
              click on a rating to continue to the next card
            </span>
          </div>
        </div>
      )}
      {!isRevealed && (
        <div className="flex justify-center">
          <Buttons.PrimaryButton onClick={() => setIsRevealed(true)}>
            Reveal Answer
          </Buttons.PrimaryButton>
        </div>
      )}
    </div>
  );
};
