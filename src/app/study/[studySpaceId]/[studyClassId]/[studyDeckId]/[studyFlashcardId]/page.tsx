"use client";

import { StudyModeFlashcard } from "./components/StudyModeFlashcard";
import { useStudyModeNavigationParams } from "@/hooks/navigation.hooks";
import { useGraphQLDeck } from "@/graphql/domain/deck.graphql";
import { useGraphQLFlashcardById } from "@/graphql/domain/flashcard.graphql";
import { Titles } from "@/app/components/common/Titles";
import { PageWrapper } from "@/app/components/common/layout/PageWrapper";

export default function StudyFlashcardPage() {
  const { studyDeckId, studyFlashcardId } = useStudyModeNavigationParams();
  const { data: dataDeck } = useGraphQLDeck(studyDeckId);
  const { data: dataFlashcard } = useGraphQLFlashcardById(studyFlashcardId);
  const deck = dataDeck?.deck;
  const flashcard = dataFlashcard?.flashcard;

  return (
    <PageWrapper title={`Studying: ${deck?.name ?? ""}`}>
      {flashcard && deck ? (
        <div className="flex flex-grow flex-col items-center justify-center">
          <StudyModeFlashcard deck={deck} flashcard={flashcard} />
        </div>
      ) : (
        <div>
          <Titles.PageTitle>couldnt find flashcard</Titles.PageTitle>
        </div>
      )}
    </PageWrapper>
  );
}
