"use client";
// requestContext.tsx
import { AxiosRequestManager } from "@/utils/manager/axiosRequestManager";
import React, { createContext, useContext, ReactNode } from "react";

interface IAppContextProps {
  children: ReactNode;
}

interface IAppContext {
  axiosRequestManager: AxiosRequestManager;
}

const AppContext = createContext<Partial<IAppContext>>({});

export const AppContextProvider: React.FC<IAppContextProps> = ({ children }) => {
  return (
    <AppContext.Provider
      value={{
        axiosRequestManager: new AxiosRequestManager(),
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

export const useAppContext = (): IAppContext => {
  const context = useContext(AppContext);
  return context as IAppContext;
};
