export enum EPdfUploadVerboseOptions {
  SHORT = "short",
  MEDIUM = "medium",
  LONG = "long",
}
