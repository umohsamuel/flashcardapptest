const getSpaceRoute = ({ spaceId }: { spaceId?: string }) => `/space/${spaceId}`;

const getClassRoute = ({ spaceId, classId }: { spaceId?: string; classId?: string }) =>
  `${getSpaceRoute({ spaceId })}/class/${classId}`;

const getDeckRoute = ({
  spaceId,
  classId,
  deckId,
}: {
  spaceId?: string;
  classId?: string;
  deckId?: string;
}) => {
  const classRoute = getClassRoute({ spaceId, classId });
  return `${classRoute}/deck/${deckId}`;
};

const getStudyFlashcardRoute = ({
  spaceId,
  classId,
  deckId,
  flashcardId,
}: {
  spaceId: string;
  classId: string;
  deckId?: string;
  flashcardId?: string;
}) => {
  return `/study/${spaceId}/${classId}/${deckId}/${flashcardId}`;
};

export const ROUTES_CONFIG = {
  dashboard: "/",
  designElements: "/design",
  getSpaceRoute,
  getClassRoute,
  getDeckRoute,
  getStudyFlashcardRoute,
};

export const API_ROUTES_CONFIG = {
  upload: "/upload",
};

export interface IRouteParams {
  spaceId?: string;
  classId?: string;
  deckId?: string;
  studySpaceId?: string;
  studyClassId?: string;
  studyDeckId?: string;
  studyFlashcardId?: string;
}
