import { useDeckId } from "@/hooks/navigation.hooks";
import { UseSuspenseQueryResult, gql, useMutation, useSuspenseQuery } from "@apollo/client";

const gqlDeckById = (id?: string) => gql`
  {
    deck(id: "${id}") {
      id
      name
      labels
      flashcards {
        id
        question
        answer
      }
    }
  }
`;

interface IUseGraphQLDeckReturn {
  deck: IGraphQLDeck;
}

export function useGraphQLDeck(
  deckidParam?: string
): UseSuspenseQueryResult<IUseGraphQLDeckReturn | undefined> {
  const deckIdContext = useDeckId();
  const deckId = deckidParam ?? deckIdContext;
  return useSuspenseQuery<IUseGraphQLDeckReturn>(gqlDeckById(deckId), {
    skip: !deckId,
  });
}

const gqlMutationDeckUpdate = gql`
  mutation UpdateDeck($id: ID!, $name: String, $image: String) {
    updateDeck(id: $id, name: $name, image: $image) {
      id
      name
      image
    }
  }
`;

interface IGraphQLUpdateDeckData {
  id: string;
  name?: string;
  image?: string;
}

export function useGraphQLUpdateDeck() {
  const [updateDeckMutation, { data, loading, error }] =
    useMutation<IGraphQLDeck>(gqlMutationDeckUpdate);

  const updateDeck = async ({ id, name, image }: Partial<IGraphQLUpdateDeckData>) => {
    try {
      return await updateDeckMutation({
        variables: { id, name, image },
      });
    } catch (err) {
      return { error: `Failed to update deck ${error?.message}` };
    }
  };

  return {
    updateDeck,
    loading,
    error,
  };
}

// ---------------------------------------------------------------------------
// ADD DECK ADD DECK ADD DECK ADD DECK ADD DECK ADD DECK ADD DECK ADD DECK ADD DECK

const gqlMutationAddDeck = gql`
  mutation AddDeck($name: String!, $labels: [String!]!) {
    addDeck(name: $name, labels: $labels) {
      id
      name
      labels
    }
  }
`;

interface IGraphQLAddDeckVariables {
  name: string;
  labels: string[];
}

export function useGraphQLAddDeck() {
  const [addDeckMutation] = useMutation(gqlMutationAddDeck);

  const addDeck = async (variables: IGraphQLAddDeckVariables) => {
    return await addDeckMutation({
      variables,
    });
  };

  return {
    addDeck,
  };
}

// ---------------------------------------------------------------------------
// DELETE DECK DELETE DECK DELETE DECK DELETE DECK DELETE DECK DELETE DECK DELETE DECK

const gqlMutationDeleteDeck = gql`
  mutation DeleteDeck($id: ID!) {
    deleteDeck(id: $id) {
      id
    }
  }
`;

export function useGraphQLDeleteDeck() {
  const [deleteDeckMutation] = useMutation(gqlMutationDeleteDeck);

  const deleteDeck = async (id: string) => {
    return await deleteDeckMutation({
      variables: { id },
    });
  };

  return {
    deleteDeck,
  };
}
