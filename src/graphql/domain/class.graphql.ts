import { UseSuspenseQueryResult, gql, useMutation, useSuspenseQuery } from "@apollo/client";
import { useParams } from "next/navigation";
import { useGraphQLSpace } from "./user.graphql";
import { IRouteParams } from "@/constants/routes.constants";
import { useClassId } from "@/hooks/navigation.hooks";

// ---------------------------------------------------------------------------
// ALL CLASSES OF USER

export function useGraphQLUserClasses() {
  const params = useParams() as IRouteParams;
  return useGraphQLSpace();
}

// ---------------------------------------------------------------------------
// GET CLASS GET CLASS GET CLASS GET CLASS GET CLASS GET CLASS GET CLASS

interface IUseGraphQLClassReturn {
  clazz: IGraphQLClass | null;
}

const gqlClassById = (id?: string) => gql`
  {
    clazz(id: "${id}") {
      id
      name
      decks {
        id
        name
        labels
        flashcards {
          id
        }
      }
    }
  }
`;

export function useGraphQLClass(
  classIdParam?: string
): UseSuspenseQueryResult<IUseGraphQLClassReturn | undefined> {
  const classId = useClassId() ?? classIdParam;
  return useSuspenseQuery<IUseGraphQLClassReturn>(gqlClassById(classId), {
    skip: !classId,
  });
}

// ---------------------------------------------------------------------------
// UPDATE CLASS UPDATE CLASS UPDATE CLASS UPDATE CLASS UPDATE CLASS UPDATE CLASS

const gqlMutationClassUpdate = gql`
  mutation UpdateClass($id: ID!, $name: String, $description: String) {
    updateClass(id: $id, name: $name, description: $description) {
      id
      name
      description
    }
  }
`;

interface IGraphQLUpdateClassData {
  id: string;
  name?: string;
  description?: string;
}

export function useGraphQLUpdateClass() {
  const [updateClassMutation, { data, loading, error }] =
    useMutation<Partial<IGraphQLClass>>(gqlMutationClassUpdate);

  const updateClass = async ({ id, name, description }: IGraphQLUpdateClassData) => {
    try {
      return await updateClassMutation({
        variables: {
          id,
          name,
          description,
        },
      });
    } catch (err) {
      return { error: `Failed to update class ${error?.message}` };
    }
  };

  return {
    updateClass,
    loading,
    error,
  };
}

// ---------------------------------------------------------------------------
// ADD CLASS ADD CLASS ADD CLASS ADD CLASS ADD CLASS ADD CLASS ADD CLASS ADD CLASS

const gqlMutationAddClass = gql`
  mutation AddClass($userId: ID!, $name: String!, $description: String) {
    addClass(userid: $userId, name: $name, description: $description) {
      id
      name
      description
    }
  }
`;

interface IGraphQLAddClassVariables {
  userId: string;
  name: string;
  description?: string;
}

export function useGraphQLAddClass() {
  const [addClassMutation] = useMutation(gqlMutationAddClass);

  const addClass = async (variables: IGraphQLAddClassVariables) => {
    return await addClassMutation({
      variables,
    });
  };

  return {
    addClass,
  };
}

// ---------------------------------------------------------------------------
// DELETE CLASS DELETE CLASS DELETE CLASS DELETE CLASS DELETE CLASS DELETE CLASS

const gqlMutationDeleteClass = gql`
  mutation DeleteClass($id: ID!) {
    deleteClass(id: $id) {
      id
    }
  }
`;

export function useGraphQLDeleteClass() {
  const [deleteClassMutation] = useMutation(gqlMutationDeleteClass);

  const deleteClass = async (id: string) => {
    return await deleteClassMutation({
      variables: { id },
    });
  };

  return {
    deleteClass,
  };
}
