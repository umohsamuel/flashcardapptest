import { UseSuspenseQueryResult, gql, useMutation, useSuspenseQuery } from "@apollo/client";

export const gqlFlashcardById = (id?: string) => gql`
  {
    flashcard(id: "${id}") {
      id
      question
      answer
    }
  }
`;

interface IUseGraphQLFlashcardByIdReturn {
  flashcard: IGraphQLFlashcard;
}

export function useGraphQLFlashcardById(
  id: string
): UseSuspenseQueryResult<IUseGraphQLFlashcardByIdReturn | undefined> {
  return useSuspenseQuery<IUseGraphQLFlashcardByIdReturn>(gqlFlashcardById(id), {
    variables: { id },
    skip: !id,
  });
}

// ---------------------------------------------------------------------------
// UPDATE FLASHCARD UPDATE FLASHCARD UPDATE FLASHCARD UPDATE FLASHCARD

const gqlMutationFlashcardUpdate = gql`
  mutation UpdateFlashcard($id: ID!, $question: String, $answer: String, $progress: Int) {
    updateFlashcard(id: $id, question: $question, answer: $answer, progress: $progress) {
      id
      question
      answer
      progress
    }
  }
`;

interface IGraphQLUpdateFlascardData {
  id: string;
  question?: string;
  answer?: string;
  progress?: number;
}

export function useGraphQLUpdateFlashcard() {
  const [updateFlashcardMutation, { data, loading, error }] = useMutation<IGraphQLFlashcard>(
    gqlMutationFlashcardUpdate
  );

  const updateFlashcard = async ({
    id,
    question,
    answer,
    progress,
  }: IGraphQLUpdateFlascardData) => {
    try {
      return await updateFlashcardMutation({
        variables: { id, question, answer, progress },
      });
    } catch (err) {
      return { error: `Failed to update flashcard ${error?.message}` };
    }
  };

  return {
    updateFlashcard,
    loading,
    error,
  };
}

// ---------------------------------------------------------------------------
// ADD FLASHCARD ADD FLASHCARD ADD FLASHCARD ADD FLASHCARD ADD FLASHCARD ADD FLASHCARD

const gqlMutationAddFlashcard = gql`
  mutation AddFlashcard($question: String!, $answer: String!) {
    addFlashcard(question: $question, answer: $answer) {
      id
      question
      answer
    }
  }
`;

interface IGraphQLAddFlashCardVariables {
  question: string;
  answer: string;
}

export function useGraphQLAddFlashcard() {
  const [addFlashcardMutation] = useMutation(gqlMutationAddFlashcard);

  const addFlashcard = async (variables: IGraphQLAddFlashCardVariables) => {
    return await addFlashcardMutation({
      variables,
    });
  };

  return {
    addFlashcard,
  };
}

// ---------------------------------------------------------------------------
// DELETE FLASHCARD DELETE FLASHCARD DELETE FLASHCARD DELETE FLASHCARD DELETE FLASHCARD

const gqlMutationDeleteFlashcard = gql`
  mutation DeleteFlashcard($id: ID!) {
    deleteFlashcard(id: $id) {
      id
    }
  }
`;

export function useGraphQLDeleteFlashcard() {
  const [deleteFlashcardMutation] = useMutation(gqlMutationDeleteFlashcard);

  const deleteFlashcard = async (id: string) => {
    return await deleteFlashcardMutation({
      variables: { id },
    });
  };

  return {
    deleteFlashcard,
  };
}
