import { useSpaceId } from "@/hooks/navigation.hooks";
import {
  UseSuspenseQueryResult,
  gql,
  useMutation,
  useSuspenseQuery,
} from "@apollo/client";

const gqlUserById = (id?: string) => gql`
  {
    user(id: "${id}") {
      id
      name
      classes {
        id
        name
        decks {
          id
          name
        }
      }
    }
  }
`;

interface IUseGraphQLUserReturn {
  user: IGraphQLUser;
}

export function useGraphQLSpace(): UseSuspenseQueryResult<IUseGraphQLUserReturn | undefined> {
  const spaceId = useSpaceId();
  return useSuspenseQuery<IUseGraphQLUserReturn>(gqlUserById(spaceId), {
    skip: !spaceId,
  });
}

// ---------------------------------------------------------------------------
// LIST USERS/LIST USERS/LIST USERS/LIST USERS/LIST USERS

const gqlListUsers = gql`
  {
    users {
      id
      name
    }
  }
`;

interface IGraphQLListUsersReturn {
  users: IGraphQLUser[];
}

export function useGraphQLUsers(): UseSuspenseQueryResult<IGraphQLListUsersReturn> {
  return useSuspenseQuery<IGraphQLListUsersReturn>(gqlListUsers);
}

// ---------------------------------------------------------------------------
// UPDATE USER/SPACE UPDATE USER/SPACE UPDATE USER/SPACE UPDATE USER/SPACE

const gqlMutationUserUpdate = gql`
  mutation UpdateUser($id: ID!, $name: String, $profilePicture: String) {
    updateUser(id: $id, name: $name, profilePicture: $profilePicture) {
      id
      name
      profilePicture
    }
  }
`;

interface IGraphQLUpdateUserData {
  id: string;
  name?: string;
  profilePicture?: string;
}

export function useGraphQLUpdateUser() {
  const [updateUserMutation, { data, loading, error }] =
    useMutation<IGraphQLUser>(gqlMutationUserUpdate);

  const updateUser = async ({ id, name, profilePicture }: IGraphQLUpdateUserData) => {
    try {
      return await updateUserMutation({
        variables: { id, name, profilePicture },
      });
    } catch (err) {
      return { error: `Failed to update user ${error?.message}` };
    }
  };

  return {
    updateUser,
    loading,
    error,
  };
}

// ---------------------------------------------------------------------------
// ADD USER/SPACE ADD USER/SPACE ADD USER/SPACE ADD USER/SPACE ADD USER/SPACE

const gqlMutationAddUser = gql`
  mutation AddUser($name: String!, $email: String!, $hasPremium: Boolean!) {
    addUser(name: $name, email: $email, hasPremium: $hasPremium) {
      id
      name
      email
      hasPremium
    }
  }
`;

interface IGraphQLAddUserVariables {
  name: string;
  email: string;
  hasPremium: boolean;
}

export function useGraphQLAddUser() {
  const [addUserMutation] = useMutation(gqlMutationAddUser);

  const addUser = async (variables: IGraphQLAddUserVariables) => {
    return await addUserMutation({
      variables,
    });
  };

  return {
    addUser,
  };
}

// ---------------------------------------------------------------------------
// DELETE USER/SPACE DELETE USER/SPACE DELETE USER/SPACE DELETE USER/SPACE

const gqlMutationDeleteUser = gql`
  mutation DeleteUser($id: ID!) {
    deleteUser(id: $id) {
      id
    }
  }
`;

export function useGraphQLDeleteUser() {
  const [deleteUserMutation] = useMutation(gqlMutationDeleteUser);

  const deleteUser = async (id: string) => {
    return await deleteUserMutation({
      variables: { id },
    });
  };

  return {
    deleteUser,
  };
}
