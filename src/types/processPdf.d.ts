interface IProcessPdfResponse
  extends Partial<IProcessPdfResponseSuccess>,
    Partial<IProcessPdfResponseError> {
  error?: string;
}

interface IProcessPdfResponseSuccess {
  success: true;
  statusMessage: string;
  deckUUIDs: string[];
  classUUID: string;
  userUUID: string;
}

interface IProcessPdfResponseError {
  success: false;
  statusMessage: string;
  errorCode?: number;
}
