type ID = string;

type Query = {
  users: IGraphQLUser[];
  user(id: ID): IGraphQLUser;
  class(id: ID): IGraphQLClass;
  deck(id: ID): IGraphQLDeck;
  flashcard(id: ID): IGraphQLFlashcard;
};

interface IGraphQLUser {
  id: string;
  name: string;
  email: string;
  hasPremium: boolean;
  classes: IGraphQLClass[];
}

interface IGraphQLFlashcard {
  id: string;
  question: string;
  answer: string;
  decks: IGraphQLDeck[];
}

interface IGraphQLClass {
  id: string;
  name: string;
  description: string;
  decks: IGraphQLDeck[];
  users: IGraphQLUser[];
}

interface IGraphQLDeck {
  id: string;
  name: string;
  image?: string; // Added since it's present in the GraphQL schema but optional
  isPublic: boolean; // Changed from Boolean to boolean for TypeScript correctness
  flashcards: IGraphQLFlashcard[];
  classes: IGraphQLClass[];
  labels: string[];
}
