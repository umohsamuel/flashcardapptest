# Flashcard Web App

This is a simple but powerful web application designed to create, manage, and use flashcards for learning and studying.

## Features

- Creation and management of flashcard decks
- Easy-to-use, mobile-friendly user interface
- Use of Redux for state management
- Tailwind CSS for sleek, responsive design
- Utilization of the Flowbite toolkit for enhanced user experience
- Robust error handling and validation
- Support for Next.js and TypeScript

## Installation

First, you'll need to clone the repository to your local machine. You can do this with the following command:

```bash
git clone https://github.com/username/flashcardappwebapp.git
```

Next, navigate into the directory:

```bash
cd flashcardappwebapp
```

Now, you can install all dependencies using yarn:

```bash
yarn install
```

Usage
To start the development server, you can run:

```bash
yarn dev
```

To build the application for production, you can run:

```bash
yarn build
```

After building the application, you can start it with:

```bash
yarn start
```

You can also lint your code using:

```bash
yarn lint
```
